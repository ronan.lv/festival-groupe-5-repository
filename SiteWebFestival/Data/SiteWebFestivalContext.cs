﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using APIFestival.Models;

namespace SiteWebFestival.Data
{
    public class SiteWebFestivalContext : DbContext
    {
        public SiteWebFestivalContext (DbContextOptions<SiteWebFestivalContext> options)
            : base(options)
        {
        }

        public DbSet<APIFestival.Models.Festival> Festival { get; set; }
    }
}
