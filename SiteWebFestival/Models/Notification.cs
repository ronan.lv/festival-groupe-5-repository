﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIFestival.Models;


namespace SiteWebFestival.Models
{
    public class Notification
    {
        public JouerSur jouer { get; set; }
        public String nomArtiste { get; set; }
    }
}
