﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using APIFestival.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SiteWebFestival.Models;


namespace SiteWebFestival.Controllers
{
    public class EspaceFestivalierController : Controller
    {
        public IActionResult Index()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("festivalierConnecte")))
            {
                int i = JsonConvert.DeserializeObject<Festivalier>(HttpContext.Session.GetString("festivalierConnecte")).Id;
                List < Modifications > listemessages = DAOAPI.Instance.GetModificationsFestivalier(i).Result;
                listemessages = (from m in listemessages where m.LuOuNon == false select m).ToList();
                if (listemessages.Count > 0)
                {
                    HttpContext.Session.SetString("notificationFestivalier", "Vous avez " + listemessages.Count + " messages non lus. Consultez vos notifications!");
                }
                return View();
               
                ICollection<Notification> listeNotification = new List<Notification>();
                

               var d = Convert.ToInt32(TempData["userID"]);
                var selection = DAOAPI.Instance.GetSelectionByFestivalierId(Convert.ToInt32(TempData["userID"])).Result;
                foreach (var select in selection)
                {
                    var jouer = DAOAPI.Instance.GetFestivalierByFestivalId(select.IdArtiste).Result;

                    foreach (var jo in jouer)
                    {
                        Notification n = new Notification();
                        var artiste = DAOAPI.Instance.GetArtiste(jo.IdArtiste).Result;
                        DateTime dt1 = jo.DateHeureDebutPerformance;
                        DateTime dt2 = DateTime.Now;
                        TimeSpan ts = dt1 - dt2;

                        if (ts.TotalMinutes > 0 && ts.TotalMinutes < 30)
                        {
                            n.nomArtiste = artiste.Nom;
                            n.jouer = jo;
                            listeNotification.Add(n);
                        }

                    }
                }
              
                //HttpContext.Session.SetString("notification" +listeJouer[i], "La performance de " + artiste.Nom + " commencera à " + jo.DateHeureDebutPerformance + " ! ");
             
                
                return View(listeNotification);
            }
            else
            {
                return RedirectToAction("Connexion", "Home");
            }
        }

        public IActionResult ListeFestivals()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("festivalierConnecte")))
            {
                ICollection<Festival> listeFestivals = _DAOAPI.Instance.GetFestivals().Result;
                return View(listeFestivals);
            }
            else
            {
                return RedirectToAction("Connexion", "Home");
            }
        }
        public IActionResult DescriptionFestival(int? id)
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("festivalierConnecte")))
            {
                if (id != null) {
                    
                    Festival festivalDeFestConnecte = DAOAPI.Instance.GetFestivalAsync(id).Result;
                    int NombreDePlaces = festivalDeFestConnecte.NombreDePlaces;
                    int NombresInscrits = DAOAPI.Instance.GetNombreInscrits(festivalDeFestConnecte.Id).Result;
                    ViewData["NombresInscrits"] = NombresInscrits;
                    return View(DAOAPI.Instance.GetFestivalAsync(id).Result);
                }
            else
                return View("ErreurFormulaireIncorrect");
            }
            else
            {
                return RedirectToAction("Connexion", "Home");
            }
        }

        public IActionResult ValidationCommande()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("festivalierConnecte")))
            {

                if (!string.IsNullOrEmpty(Request.Form["dateDebutFormule"]) && !string.IsNullOrEmpty(Request.Form["dateFinFormule"]) && !string.IsNullOrEmpty(Request.Form["idFestival"]))
                {
                    //Toutes les données nécessaire sur la page sont affichés dans des ViewDatas (bonne pratique je crois, à vérifier avec le tuteur technique)
                    Festivalier festConnecte = JsonConvert.DeserializeObject<Festivalier>(HttpContext.Session.GetString("festivalierConnecte"));
                    Festival festivalDeFestConnecte = _DAOAPI.Instance.GetFestivalAsync(Int32.Parse(Request.Form["idFestival"])).Result;
                    Billet billetFestivalier = new Billet();
                    ViewData["festConnecte"] = festConnecte;
                    ViewData["festivalDeFestConnecte"] = festivalDeFestConnecte;
                    billetFestivalier.IdFestivalier = festConnecte.Id;
                    billetFestivalier.IdFestival = festivalDeFestConnecte.Id;
                    billetFestivalier.DebutFormule = DateTime.Parse(Request.Form["dateDebutFormule"]);
                    billetFestivalier.FinFormule = DateTime.Parse(Request.Form["dateFinFormule"]);
                    billetFestivalier.DateAchat = DateTime.Now;
                    billetFestivalier.Montant = (billetFestivalier.FinFormule.Subtract(billetFestivalier.DebutFormule).Days + 1) * festivalDeFestConnecte.TarifJournee;
                    billetFestivalier.Validation = 3;
                    ViewData["billetFestivalier"] = billetFestivalier;

                  
                    
                    return View();
                }
                else
                {
                    return View("ErreurFormulaireIncorrect");
                }
            }
            else
            {
                return RedirectToAction("Connexion", "Home");
            }
        }
        public IActionResult NotificationsFestivalier()
        {
            Festivalier festConnecte = JsonConvert.DeserializeObject<Festivalier>(HttpContext.Session.GetString("festivalierConnecte"));
            List<Modifications> Listemessages = new List<Modifications>();
            Listemessages=DAOAPI.Instance.GetModificationsFestivalier(festConnecte.Id).Result;

            ViewData["Messages"] = Listemessages;
            ViewData["Contenu"] = "";
            if (Request.Method == "POST")
            {
                int n = Int32.Parse(Request.Form["Message"]);
                var m = (from mes in Listemessages where mes.Id == n select mes).First();
                m.LuOuNon = true;
                DAOAPI.Instance.PutModifications(m).Wait();
                ViewData["Contenu"] = m.Message;
            }
            return View();

        }

        public IActionResult EnregistrementBillet()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("festivalierConnecte")))
            {
                if (!string.IsNullOrEmpty(Request.Form["idFestival"]) && !string.IsNullOrEmpty(Request.Form["debutFormule"])
                   && !string.IsNullOrEmpty(Request.Form["finFormule"]) && !string.IsNullOrEmpty(Request.Form["montant"]))
                {
                    Festivalier festConnecte = JsonConvert.DeserializeObject<Festivalier>(HttpContext.Session.GetString("festivalierConnecte"));
                    Festival festivalDeFestConnecte = _DAOAPI.Instance.GetFestivalAsync(Int32.Parse(Request.Form["idFestival"])).Result;

                    Billet billetFestivalier = new Billet();
                    billetFestivalier.IdFestivalier = festConnecte.Id;
                    billetFestivalier.IdFestival = Int32.Parse(Request.Form["idFestival"]);
                    billetFestivalier.DebutFormule = DateTime.Parse(Request.Form["debutFormule"]);
                    billetFestivalier.FinFormule = DateTime.Parse(Request.Form["finFormule"]);
                    billetFestivalier.Montant = float.Parse(Request.Form["montant"]);
                    billetFestivalier.DateAchat = DateTime.Now;
                    billetFestivalier.Validation = 3;

                    _ = _DAOAPI.Instance.AjoutBilletAsync(billetFestivalier);
                    HttpContext.Session.SetString("notificationFestivalier", "Vous êtes inscrit pour " + festivalDeFestConnecte.Nom + " ! ");

                    return RedirectToAction("Index", "EspaceFestivalier");
                }

                return View();
            }
            else
            {
                return RedirectToAction("Connexion", "Home");
            }
        }
        public IActionResult MesFestivals()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("festivalierConnecte")))
            {
                ICollection<Festival> listeFestivals1 = DAOAPI.Instance.GetFestivals().Result;
                ICollection<Billet> listeBillets = DAOAPI.Instance.GetBillets().Result;
                int i = JsonConvert.DeserializeObject<Festivalier>(HttpContext.Session.GetString("festivalierConnecte")).Id;
                listeBillets = (from b in listeBillets where b.IdFestivalier == i select b).ToList<Billet>();
                List<Festival> listeFestivals = new List<Festival>();
                foreach (Festival f in listeFestivals1)
                {
                    foreach (Billet b in listeBillets)
                    {
                        if (f.Id == b.IdFestival)
                        {
                            if (!listeFestivals.Contains(f))
                            {
                                listeFestivals.Add(f);
                            }
                            
                        }
                    }
                }
                ViewData["listeMesFestivals"] = listeFestivals;

                return View();
            }
            else
            {
                return RedirectToAction("Connexion", "Home");
            }
            
        }

        public IActionResult MaSelection(int? idFestival)
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("festivalierConnecte")))
            {
                if(idFestival != null && JsonConvert.DeserializeObject<List<Festival>>(HttpContext.Session.GetString("listeFestivals")).Exists(x => x.Id == idFestival))
                {
                    int idFestivalier = JsonConvert.DeserializeObject<Festivalier>(HttpContext.Session.GetString("festivalierConnecte")).Id;

                    if (Request.Method.Equals("POST"))
                    {
                        String[] tabSelectionFestivalier = ((string)Request.Form["selectionFestivalier"]).Split("-");

                        List<Selection> listeSelectionsEnBDD = (from selection in DAOAPI.Instance.GetSelections().Result.ToList<Selection>() where selection.IdFestivalier == idFestivalier select selection).ToList();
                        //On vérifie l'existence des artistes passés en paramètre en BDD, et on les ajoute si besoin
                        for (int i = 1; i < tabSelectionFestivalier.Length; i++)
                        {
                            if (!listeSelectionsEnBDD.Exists(x => x.IdArtiste == Int32.Parse(tabSelectionFestivalier[i])))
                            {
                                Selection selection = new Selection();
                                selection.IdFestivalier = idFestivalier;
                                Debug.WriteLine(tabSelectionFestivalier[i]);
                                Debug.WriteLine("-------------------");
                                selection.IdArtiste = Int32.Parse(tabSelectionFestivalier[i]);
                                DAOAPI.Instance.AjoutSelectionAsync(selection).Wait();
                            }
                        }

                        //On supprime ensuite les artistes stockés en bdd mais non passés en paramètre
                        foreach (Selection selection in listeSelectionsEnBDD)
                        {
                            if (!tabSelectionFestivalier.Contains(selection.IdArtiste.ToString()))
                            {
                                DAOAPI.Instance.DeleteSelectionAsync(selection).Wait();
                            }
                        }

                        HttpContext.Session.SetString("notificationFestivalier", "Votre sélection a bien été mise à jour.");

                    }


                    Festival festivalDuFestivalier = DAOAPI.Instance.GetFestivalAsync(idFestival).Result;
                    List<Scene> listeScenes = (from scene in DAOAPI.Instance.GetScenes().Result where scene.IdFestival == festivalDuFestivalier.Id select scene).ToList();
                    List<JouerSur> listeJouerSurs = (from jouerSur in DAOAPI.Instance.GetJouerSurs().Result.ToList() join scene in listeScenes on jouerSur.IdScn equals scene.Id select jouerSur).ToList();
                    List<Artiste> listeArtistes = (from artiste in DAOAPI.Instance.GetArtistes().Result.ToList() join jouerSur in listeJouerSurs on artiste.Id equals jouerSur.IdArtiste select artiste).ToList();
                    List<Selection> listeSelections = (from selection in DAOAPI.Instance.GetSelections().Result.ToList() where selection.IdFestivalier == idFestivalier select selection).ToList();

                    ViewData["listeScenes"] = listeScenes;
                    ViewData["listeJouerSurs"] = listeJouerSurs;
                    ViewData["listeArtistes"] = listeArtistes;
                    ViewData["listeSelections"] = listeSelections;

                    ViewData["nbrDeJoursDuFestival"] = festivalDuFestivalier.DateDeFin.Subtract(festivalDuFestivalier.DateDeDebut).Days + 1;
                    ViewData["dateDeDebutFestival"] = festivalDuFestivalier.DateDeDebut;


                    return View();
                }
                else
                {
                    ViewData["pageCible"] = "MaSelection";
                    ViewData["listeFestivals"] = JsonConvert.DeserializeObject<List<Festival>>(HttpContext.Session.GetString("listeFestivals"));
                    return View("ChoixFestival");
                }
               
            }
            else
            {
                return RedirectToAction("Connexion", "Home");
            }
        }

        public IActionResult AjouterAmi()
        {


            if (Request.Method == "POST")
            {
                Festivalier festConnecte = JsonConvert.DeserializeObject<Festivalier>(HttpContext.Session.GetString("festivalierConnecte"));
                String email = Request.Form["email"];
                Festivalier festivalier = new Festivalier();
                List<Festivalier> listeFestivaliers = _DAOAPI.Instance.GetFestivaliers().Result;
                festivalier = (from fest in listeFestivaliers where fest.AdresseMail.Equals(email) select fest).First<Festivalier>();
                ViewData["Festivalier"] = festivalier;


                RelationAmicale relationAmicale = new RelationAmicale();
                relationAmicale.IdAmi = festivalier.Id;
                relationAmicale.IdFestivalier = festConnecte.Id;
                relationAmicale.dateAjoutAmi = DateTime.Now;
                relationAmicale.Statut = 0;
                _ = DAOAPI.Instance.AjoutRelationAmicaleAsync(relationAmicale);
                ViewData["relationAmicale"] = relationAmicale;

            }


            return View();
        }
        public IActionResult MesAmis()
        {
            Festivalier festConnecte = JsonConvert.DeserializeObject<Festivalier>(HttpContext.Session.GetString("festivalierConnecte"));
<<<<<<< HEAD
            List<RelationAmicale> listeRelationAmicale = DAOAPI.Instance.GetRelationAmicales().Result;
            List<RelationAmicale> listeAttente = new List<RelationAmicale>();
            List<RelationAmicale> listeValider = new List<RelationAmicale>();
            List<Festivalier> listeFestivaliers = DAOAPI.Instance.GetFestivaliers().Result;

            foreach (RelationAmicale relation in listeRelationAmicale)
            {
                if (relation.Statut == 1)
                {
                    //Festivalier festivalier = new Festivalier();

                    // festivalier = (from fest in listeFestivaliers where fest.Id.Equals(relation.IdFestivalier) select fest).First<Festivalier>();

                    listeValider.Add(relation);
                }
                else if (relation.Statut == 0)
                {
                    //Festivalier festivalier = new Festivalier();

                    // festivalier = (from fest in listeFestivaliers where fest.Id.Equals(relation.IdAmi) select fest).First<Festivalier>();

                    listeAttente.Add(relation);
                }

                    //festivalier = (from fest in listeFestivaliers where fest.Id.Equals(relation.IdFestivalier) select fest).First<Festivalier>();
                   




            }
            ViewData["ListeAttente"] = listeAttente;
            ViewData["ListeValider"] = listeValider;
            ViewData["ListeFestivalier"] = listeFestivaliers;
            ViewData["FestConnecte"] = festConnecte;


            return View();

        }
        public IActionResult ValiderAmi(int id)
        {
            RelationAmicale relationAmicale = new RelationAmicale();
            List<RelationAmicale> listeRelationAmicale = _DAOAPI.Instance.GetRelationAmicales().Result;
            relationAmicale = (from rel in listeRelationAmicale where rel.Id.Equals(id) select rel).First<RelationAmicale>();
            relationAmicale.Statut = 1;
<<<<<<< HEAD
            _ = DAOAPI.Instance.PutRelationAmicale(relationAmicale);
            return Redirect("/EspaceFestivalier/MesAmis");

        }

        public IActionResult RefuserAmi(int id)
        {
            RelationAmicale relationAmicale = new RelationAmicale();
            List<RelationAmicale> listeRelationAmicale = _DAOAPI.Instance.GetRelationAmicales().Result;
            relationAmicale = (from rel in listeRelationAmicale where rel.Id.Equals(id) select rel).First<RelationAmicale>();
            relationAmicale.Statut = 2;
            _ = _DAOAPI.Instance.PutRelationAmicale(relationAmicale);
            return Redirect("/EspaceFestivalier/MesAmis");

        }
        public IActionResult Deconnexion()
        {
            int i = JsonConvert.DeserializeObject<Festivalier>(HttpContext.Session.GetString("festivalierConnecte")).Id;
            Consultation cons = new Consultation();
            cons.Date_enregistrement = System.DateTime.Now;
            var listebillets = DAOAPI.Instance.GetBillets().Result;
            int idfestival = (from b in listebillets where b.IdFestivalier == i select b).First().IdFestival;
            cons.IdFestival = idfestival;
            DAOAPI.Instance.AjoutConsultationAsync(cons).Wait();
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
       
        

        }

    
}


