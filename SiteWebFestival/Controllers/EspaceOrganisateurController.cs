﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIFestival.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using SiteWebFestival.Models;
using Newtonsoft.Json;
using System.Diagnostics;

namespace SiteWebFestival.Controllers
{
    public class EspaceOrganisateurController : Controller
    {
        public IActionResult Index()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("organisateurConnecte")))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Deconnexion", "EspaceOrganisateur");
            }
        }


        public IActionResult MonFestival()
        {
            //Vérifie que l'organisateur est  bien connecté et renvoye l'utilisateur vers la page de connexion sinon
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("organisateurConnecte")))
            {
                Organisateur orgaConnecte = JsonConvert.DeserializeObject<Organisateur>(HttpContext.Session.GetString("organisateurConnecte"));
                //En effet, lors son inscription, un organisateur voit son champ "IdFestival" passer à -1. Cela signifie que son festvial n'est pas encore
                //crée et que donc cet organisateur n'est pour l'instant lié à aucun festival.
                if (orgaConnecte.IdFestival == -1)
                {
                    ViewBag.NomOrg = orgaConnecte.Nom;
                    ViewBag.PrenomOrg = orgaConnecte.Prenom;
                    ViewBag.MailOrg = orgaConnecte.AdresseMail;
                    ViewBag.TelOrg = orgaConnecte.NumeroTelephone;
                    return View("MonFestival/FestivalPasEncoreCree");
                }
                //Dans le cas ou IdFestival est != -1 alors idFestival est nécessairement égal à l'id d'un festival venant d'être crée. 
                //Cependant, il faut ensuite (étape suivante) vérifier si le festival reliè à cet id est complet ou bien si l'on parle uniquement
                //du squelette.
                else
                {
                    Festival festivalDeOrgaConnecte = _DAOAPI.Instance.GetFestivalAsync(orgaConnecte.IdFestival).Result;
                    //ici le festival à bien été crée mais uniquement le squelette. On affiche donc la page permettant de remplir les modalités.
                    if (festivalDeOrgaConnecte.RempliOuNon == false)
                    {
                        ViewBag.NomFestival = festivalDeOrgaConnecte.Nom;
                        ViewBag.Description = festivalDeOrgaConnecte.Description;
                        ViewBag.UrlLogo = festivalDeOrgaConnecte.UrlLogo;
                        ViewBag.Lieu = festivalDeOrgaConnecte.Lieu;
                       
                        //Ici on est dans le cas ou l'on a remplis les modalités du formaulaire et que l'on souhaite les enregistrer via l'API et PUT
                        if (Request.Method == "POST")
                        {
                            Festival festivalFinal = new Festival();
                            festivalFinal.Nom = festivalDeOrgaConnecte.Nom;
                            festivalFinal.UrlLogo = festivalDeOrgaConnecte.UrlLogo;
                            festivalFinal.Description = festivalDeOrgaConnecte.Description;
                            festivalFinal.Lieu = festivalDeOrgaConnecte.Lieu;
                            festivalFinal.DateDeDebut = DateTime.Parse(Request.Form["date1"]);
                            festivalFinal.DateDeFin = DateTime.Parse(Request.Form["date2"]);
                            festivalFinal.TarifJournee = (float)Convert.ToDouble(Request.Form["tarif"]);
                            festivalFinal.NombreDePlaces = (int)Convert.ToDouble(Request.Form["nombre"]);
                            festivalFinal.Hebergement = Request.Form["logement"];

                            festivalFinal.RempliOuNon = true;
                            festivalFinal.Id = (int)orgaConnecte.IdFestival;

                            //View Bag
                            ViewBag.Lieu = festivalFinal.Lieu;
                            ViewBag.DateDeDebut = festivalFinal.DateDeDebut;
                            ViewBag.DateDeFin = festivalFinal.DateDeFin;
                            ViewBag.TarifJournée = festivalFinal.TarifJournee;
                            ViewBag.NombreDePlaces = festivalFinal.NombreDePlaces;
                            ViewBag.Hebergement = festivalFinal.Hebergement;
                            ViewBag.RempliOuNon = festivalFinal.RempliOuNon;
                            ViewBag.Idf = TempData["Idfestival"];

                            //mettre dans le BDD


                            _ = _DAOAPI.Instance.PutFestivals(orgaConnecte.IdFestival, festivalFinal);

                            //Recharge la page afin que l'utilisateur soit dans le cas ou RempliOuNon == true
                            return RedirectToAction("MonFestival", "EspaceOrganisateur");
                        }
                        else
                        {
                            return View("MonFestival/RemplirModalites");
                        }
                    }
                    //Ici on est dans le cas ou RempliOuNon == true donc le cas ou le festival est bien crée.
                    else
                    {
                        int idOrganisateur = JsonConvert.DeserializeObject<Organisateur>(HttpContext.Session.GetString("organisateurConnecte")).Id;
                        Organisateur nouveauOrganisateur = DAOAPI.Instance.GetOrganisateurs(idOrganisateur).Result;
                        int idfestival = nouveauOrganisateur.IdFestival;
                        Festival NouveauFestival = new Festival();
                        NouveauFestival = DAOAPI.Instance.GetFestivalAsync(idfestival).Result;
                        //Récupération des nombres de places
                        int NombreDePlaces = NouveauFestival.NombreDePlaces;
                        int NombresInscrits = DAOAPI.Instance.GetNombreInscrits(idfestival).Result;
                        int NombresDePlacesRestants = NombreDePlaces - NombresInscrits;
                        ViewData["NombresInscrits"] = NombresInscrits;
                        ViewData["NombresDePlacesRestants"] = NombresDePlacesRestants;
                        ViewData["festivalDeOrgaConnecte"] = DAOAPI.Instance.GetFestivalAsync(orgaConnecte.IdFestival).Result;
                        return View("MonFestival/AffichageFestival");
                    }

                    
                }
            }
            else
            {
                return RedirectToAction("Deconnexion", "EspaceOrganisateur");
            }
        }
        public IActionResult GererProgrammation()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("organisateurConnecte")))
            {
                int idFestivalDeOrgaConnecte = JsonConvert.DeserializeObject<Organisateur>(HttpContext.Session.GetString("organisateurConnecte")).IdFestival;
                if (idFestivalDeOrgaConnecte != -1)
                {
                    Festival festivalDeOrgaConnecte = _DAOAPI.Instance.GetFestivalAsync(idFestivalDeOrgaConnecte).Result;
                    ICollection<Scene> listeToutesLesScenes = _DAOAPI.Instance.GetScenes().Result;
                    ICollection<Artiste> listeToutLesArtistes = _DAOAPI.Instance.GetArtistes().Result;
                    ICollection<JouerSur> listeJouerSurs = _DAOAPI.Instance.GetJouerSurs().Result;

                    
                    List<Scene> listeScenes = (from scene in listeToutesLesScenes where scene.IdFestival == idFestivalDeOrgaConnecte select scene).ToList();

                    ViewData["nbrDeJoursDuFestival"] = festivalDeOrgaConnecte.DateDeFin.Subtract(festivalDeOrgaConnecte.DateDeDebut).Days + 1;
                    ViewData["dateDeDebutFestival"] = festivalDeOrgaConnecte.DateDeDebut;

                    if (Request.Method.Equals("POST"))
                    {
                        //Ajout d'un artiste
                        if (!string.IsNullOrEmpty(Request.Form["nomArtiste"]) && !string.IsNullOrEmpty(Request.Form["paysOrigine"])
                            && !string.IsNullOrEmpty(Request.Form["genreMusical"]) && !string.IsNullOrEmpty(Request.Form["heureDebut"])
                            && !string.IsNullOrEmpty(Request.Form["heureFin"]) && !string.IsNullOrEmpty(Request.Form["urlExtraitMusical"])
                            && !string.IsNullOrEmpty(Request.Form["descriptionArtiste"]) && this.Request.Form.Files != null && !string.IsNullOrEmpty(Request.Form["idScene"])
                            && !string.IsNullOrEmpty(Request.Form["numJour"]))
                        {
                            Artiste artiste = new Artiste();
                            JouerSur jouerSur = new JouerSur();
                            artiste.Nom = Request.Form["nomArtiste"];
                            artiste.PaysOrigine = Request.Form["paysOrigine"];
                            artiste.Style = Request.Form["genreMusical"];
                            artiste.UrlExtraitMusical = Request.Form["urlExtraitMusical"];
                            artiste.Descriptif = Request.Form["descriptionArtiste"];
                            artiste.UrlPhoto = "../images/artistes/" + artiste.Nom.Replace(" ", "_") + ".jpg";

                            jouerSur.IdScn = Int32.Parse(Request.Form["idScene"]);
                            jouerSur.DateHeureDebutPerformance = festivalDeOrgaConnecte.DateDeDebut.AddDays(Double.Parse(Request.Form["numJour"]) - 1);
                            jouerSur.DateHeureDebutPerformance = jouerSur.DateHeureDebutPerformance.Add(TimeSpan.Parse(Request.Form["heureDebut"]));
                            jouerSur.DateHeureFinPerformance = festivalDeOrgaConnecte.DateDeDebut.AddDays(Double.Parse(Request.Form["numJour"]) - 1);
                            jouerSur.DateHeureFinPerformance = jouerSur.DateHeureFinPerformance.Add(TimeSpan.Parse(Request.Form["heureFin"]));

                            //Permet de vérifier que l'heure de début de la prestation est bien inférieur à l'heure de fin de prestation
                            if (jouerSur.DateHeureDebutPerformance.CompareTo(jouerSur.DateHeureFinPerformance) < 0)
                            {
                                Boolean horairesPrestationArtisteOk = true;
                                List<JouerSur> listeJouerSursDeLaMemeSceneEtDeLaMemeJournee = (from jouerSurQuelconque in listeJouerSurs
                                                                                               where jouerSurQuelconque.IdScn == jouerSur.IdScn
                                                                                               && (jouerSurQuelconque.DateHeureDebutPerformance.Day == festivalDeOrgaConnecte.DateDeDebut.Day + Int32.Parse(Request.Form["numJour"]) - 1)
                                                                                               select jouerSurQuelconque).ToList<JouerSur>();
                                /*
                                 * Soit a la date/heure de début de la prestation fournit par l'utilisateur et b la fin de cette prestation (donc a < b )
                                 * Soit A la date/heure de début d'une prestation quelconque de la journée et B la fin de cette prestation (donc A < B )
                                 * Dans un cas normal d'utilisation on a a < b <= A < B ou A < B =< a < b
                                 * (Afin de simplifier les instructions on va uniquement vérifier que :
                                 *     b =< A
                                 *     B =< a 
                                 * Etant donné qu'on a nécesairement A < B et que l'on vérifie plus haut que a < b
                                 * Ensuite pour détecter si il y a une erreur, on passera la variable horairesPrestationArtisteOk à false si l'inverse de notre vérification est vrai (c'est à dire si l'on se trouve dans un cas problèmatique)
                                 */

                                foreach (JouerSur jouerSurQuelconque in listeJouerSursDeLaMemeSceneEtDeLaMemeJournee)
                                {
                                    if (!(jouerSur.DateHeureFinPerformance.CompareTo(jouerSurQuelconque.DateHeureDebutPerformance) <= 0
                                        || jouerSurQuelconque.DateHeureFinPerformance.CompareTo(jouerSur.DateHeureDebutPerformance) <= 0))
                                        horairesPrestationArtisteOk = false;
                                }

                                if (horairesPrestationArtisteOk)
                                {
                                    //Permet d'enregistrer l'image entrée par l'utilisateur dans wwwroot/images/artistes/
                                    Request.Form.Files["dataPhoto"].CopyTo(new FileStream("../SiteWebFestival/wwwroot//images/artistes/" + artiste.Nom.Replace(" ", "_") + ".jpg", FileMode.Create));

                                    _DAOAPI.Instance.AjoutArtisteAsync(artiste).Wait();
                                    jouerSur.IdArtiste = _DAOAPI.Instance.GetArtistes().Result[_DAOAPI.Instance.GetArtistes().Result.Count - 1].Id;
                                    _DAOAPI.Instance.AjoutJouerSurAsync(jouerSur).Wait();

                                    ViewData["messageSucces"] = "L'artiste a bien été ajouté !";
                                }
                                else
                                {
                                    ViewData["messageAttention"] = "Impossible d'ajouter l'artiste, sa plage horaire dépasse sur une autre prestation.";
                                }
                            }
                            else
                            {
                                ViewData["messageAttention"] = "Impossible d'ajouter l'artiste, l'heure de début est supérieure à l'heure de fin";

                            }
                        }
                        //Ajout d'une scène
                        else if (!string.IsNullOrEmpty(Request.Form["nomScene"]) && !string.IsNullOrEmpty(Request.Form["nbrPlaces"])
                                && !string.IsNullOrEmpty(Request.Form["description"]) && !string.IsNullOrEmpty(Request.Form["accessibilite"]))
                        {
                            Scene scene = new Scene();
                            scene.NomScene = Request.Form["nomScene"];
                            scene.NombreDePlaces = Int32.Parse(Request.Form["nbrPlaces"]);
                            scene.DescriptionScene = Request.Form["description"];
                            scene.Accessibilite = Request.Form["accessibilite"];
                            scene.IdFestival = idFestivalDeOrgaConnecte;

                            if (scene.NombreDePlaces <= festivalDeOrgaConnecte.NombreDePlaces)
                            {
                                _DAOAPI.Instance.AjoutSceneAsync(scene).Wait();
                                ViewData["messageSucces"] = "Votre scène a bien été ajoutée !";
                            }
                            else
                            {
                                ViewData["messageAttention"] = "Impossible d'ajouter une scène possèdant une capacité d'accueil supérieure à la capacité totale du festival.";
                            }
                        }
                        //Suppression d'un artiste
                        else if (!string.IsNullOrEmpty(Request.Form["idArtisteASupprimer"]))
                        {
                            Artiste artisteASupprimer = (from artiste in listeToutLesArtistes where artiste.Id == Int32.Parse(Request.Form["idArtisteASupprimer"]) select artiste).First<Artiste>();
                            JouerSur jouerSurASupprimer = (from jouerSur in listeJouerSurs where jouerSur.IdArtiste == Int32.Parse(Request.Form["idArtisteASupprimer"]) select jouerSur).First<JouerSur>();
                            _DAOAPI.Instance.DeleteArtisteAsync(artisteASupprimer).Wait();
                            _DAOAPI.Instance.DeleteJouerSurAsync(jouerSurASupprimer).Wait();
                            FileInfo file = new FileInfo("wwwroot/" + artisteASupprimer.UrlPhoto.Substring(2));
                            file.Delete();
                            ViewData["messageSucces"] = artisteASupprimer.Nom + " a bien été supprimé de la programmation.";

                            var festivNotif = DAOAPI.Instance.GetFestivalierSelection(artisteASupprimer).Result;
                            //Ajout du message de modification

                            string message = "Votre Artiste" + artisteASupprimer.Nom + "ne participera plus au festival";

                            if (festivNotif != null)
                            {
                                foreach (var f in festivNotif)
                                {
                                    Modifications modif = new Modifications();
                                    modif.FestivalierId = f.Id;
                                    modif.Message = message;
                                    DAOAPI.Instance.AjoutModificationAsync(modif).Wait();
                                }
                            }

                        }
                        //Modification d'un artiste
                        else if (!string.IsNullOrEmpty(Request.Form["nomArtiste"]) && !string.IsNullOrEmpty(Request.Form["paysOrigine"])
                            && !string.IsNullOrEmpty(Request.Form["genreMusical"]) && !string.IsNullOrEmpty(Request.Form["heureDebut"])
                            && !string.IsNullOrEmpty(Request.Form["heureFin"]) && !string.IsNullOrEmpty(Request.Form["urlExtraitMusical"])
                            && !string.IsNullOrEmpty(Request.Form["descriptionArtiste"]) && !string.IsNullOrEmpty(Request.Form["idArtisteAModifier"]))
                        {
                            Artiste artisteAModifier = new Artiste();
                            artisteAModifier.Id = Int32.Parse(Request.Form["idArtisteAModifier"]);
                            artisteAModifier.Nom = Request.Form["nomArtiste"];
                            artisteAModifier.PaysOrigine = Request.Form["paysOrigine"];
                            artisteAModifier.Style = Request.Form["genreMusical"];
                            artisteAModifier.UrlExtraitMusical = Request.Form["urlExtraitMusical"];
                            artisteAModifier.Descriptif = Request.Form["descriptionArtiste"];
                            artisteAModifier.UrlPhoto = (from artiste in listeToutLesArtistes where artiste.Id == artisteAModifier.Id select artiste).First<Artiste>().UrlPhoto;

                            JouerSur jouerSurAModifier = (from jouerSur in listeJouerSurs where jouerSur.IdArtiste == artisteAModifier.Id select jouerSur).First<JouerSur>();
                            jouerSurAModifier.DateHeureDebutPerformance = jouerSurAModifier.DateHeureDebutPerformance.Subtract(jouerSurAModifier.DateHeureDebutPerformance.TimeOfDay).Add(TimeSpan.Parse(Request.Form["heureDebut"]));
                            jouerSurAModifier.DateHeureFinPerformance = jouerSurAModifier.DateHeureFinPerformance.Subtract(jouerSurAModifier.DateHeureFinPerformance.TimeOfDay).Add(TimeSpan.Parse(Request.Form["heureFin"]));


                            //Permet de vérifier que l'heure de début de la prestation est bien inférieur à l'heure de fin de prestation
                            if (jouerSurAModifier.DateHeureDebutPerformance.CompareTo(jouerSurAModifier.DateHeureFinPerformance) < 0)
                            {
                                Boolean horairesPrestationArtisteOk = true;
                                List<JouerSur> listeJouerSursDeLaMemeSceneEtDeLaMemeJournee = (from jouerSurQuelconque in listeJouerSurs
                                                                                               where jouerSurQuelconque.IdScn == jouerSurAModifier.IdScn
                                                                                               && (jouerSurQuelconque.DateHeureDebutPerformance.Day == jouerSurAModifier.DateHeureDebutPerformance.Day)
                                                                                               && jouerSurQuelconque.Id != jouerSurAModifier.Id
                                                                                               select jouerSurQuelconque).ToList<JouerSur>();
                                /*
                                 * On contrôle que les horaires rentrées par l'utilisateur sont corrects. Le même contrôle est réalisé plus haut dans le code
                                 * (lors de l'ajout d'un artiste) et son explication y est détaillé.
                                 */

                                foreach (JouerSur jouerSurQuelconque in listeJouerSursDeLaMemeSceneEtDeLaMemeJournee)
                                {
                                    if (!(jouerSurAModifier.DateHeureFinPerformance.CompareTo(jouerSurQuelconque.DateHeureDebutPerformance) <= 0
                                        || jouerSurQuelconque.DateHeureFinPerformance.CompareTo(jouerSurAModifier.DateHeureDebutPerformance) <= 0))
                                        horairesPrestationArtisteOk = false;
                                }

                                if (horairesPrestationArtisteOk)
                                {
                                    if (Request.Form.Files["dataPhoto"] != null)
                                    {
                                        FileInfo file = new FileInfo("wwwroot/" + artisteAModifier.UrlPhoto.Substring(2));
                                        file.Delete();
                                        //Le nom est traité de manière un peu différente car si l'on chage le contenu de la photo mais que l'on garde le même nom, c'est l'image en 
                                        //cache qui sera affichée.
                                        String nomPhoto = artisteAModifier.Nom.Replace(" ", "_") + Request.Form.Files["dataPhoto"].Length + ".jpg";
                                        artisteAModifier.UrlPhoto = "../images/artistes/" + nomPhoto;
                                        Request.Form.Files["dataPhoto"].CopyTo(new FileStream("../SiteWebFestival/wwwroot//images/artistes/" + nomPhoto, FileMode.Create));
                                    }
                                    //On recupère les anciennes versions du programme, de l'artiste et le scène pour pouvoir trouver les modifications plutard
                                    Artiste a = DAOAPI.Instance.GetArtiste(artisteAModifier.Id).Result;
                                    JouerSur j = (from jo in DAOAPI.Instance.GetJouerSurs().Result where jo.Id == jouerSurAModifier.Id select jo).FirstOrDefault();
                                    //JouerSur j = (from jo in listeJouerSurs where jo.Id == jouerSurAModifier.Id select jo).FirstOrDefault();
                                    Scene sc = DAOAPI.Instance.GetScene(j.IdScn).Result;

                                    _DAOAPI.Instance.PutArtiste(artisteAModifier).Wait();
                                    _DAOAPI.Instance.PutJouerSur(jouerSurAModifier).Wait();

                                    ViewData["messageSucces"] = artisteAModifier.Nom + " a bien été modifié.";
                                    
                                    //LISTE DES FESTIVALIERS A NOTIFIER

                                    var festivNotif = DAOAPI.Instance.GetFestivalierSelection(a).Result;

                                    //Ajout du message de modification
                                    string message = "Voici vos modifications du "+ System.DateTime.Now.ToString("dd/MM/yyyy")+".\n \n";
                                    int n = 0;
                                    if (a.Nom != artisteAModifier.Nom)
                                    {
                                        n = 1;
                                        message = message + "Concernant l'artiste: " + a.Nom+ "."+"\n \n"+"Il s'appelle désormais " +  artisteAModifier.Nom + ".\n";
                                    }
                                    if (a.PaysOrigine != artisteAModifier.PaysOrigine)
                                    {
                                        n = 1;
                                        if (message.Contains("Concernant l'artiste: " + a.Nom + "." + "\n \n"))
                                        {
                                            message = message + "Le pays d'origine est maintenant: " + artisteAModifier.PaysOrigine + ".\n";
                                        }
                                        else
                                        {
                                            message = message + "Concernant l'artiste: " + a.Nom + ".\n \n" + "Le pays d'origine est maintenant: " + artisteAModifier.PaysOrigine + ".\n";
                                        }
                                        
                                    }
                                    if (a.Style != artisteAModifier.Style)
                                    {
                                        n = 1;
                                        if (message.Contains("Concernant l'artiste: " + a.Nom + "." + "\n \n"))
                                        {
                                            message = message + "Son nouveau genre sera désormais : " + artisteAModifier.Style + ".\n";
                                        }
                                        else
                                        {
                                            message = message + "Concernant l'artiste: " + a.Nom + ".\n \n" + "Son nouveau genre est : " + artisteAModifier.Style + ".\n";
                                        }
                                    }
                                    //Fin ajout message de modification de l'artiste

                                    //Message modification des horaires

                                    if (j.DateHeureDebutPerformance != jouerSurAModifier.DateHeureDebutPerformance && j.DateHeureFinPerformance == jouerSurAModifier.DateHeureFinPerformance)
                                    {
                                        n = 1;
                                        message = message + "En ce qui concerne les horaires de passage: \n \n";
                                        var h = jouerSurAModifier.DateHeureDebutPerformance;
                                        message = message + "Votre artiste commencera plutôt le: " + h.ToString("dd/MM/yyyy") +" sur la scène "+ sc.NomScene+ " , à " + h.ToString("HH:mm") +".\n";
                                    }

                                    if (j.DateHeureFinPerformance != jouerSurAModifier.DateHeureFinPerformance && j.DateHeureDebutPerformance == jouerSurAModifier.DateHeureDebutPerformance)
                                    {
                                        n = 1;
                                        message = message + "En ce qui concerne les horaires de passage: \n \n";
                                        var h = jouerSurAModifier.DateHeureDebutPerformance;
                                        var f = jouerSurAModifier.DateHeureFinPerformance;
                                        message = message + "Votre artiste commencera le: " + h.ToString("dd/MM/yyyy") + " sur la scène " + sc.NomScene + " comme prévu à " + h.ToString("HH:mm") +" mais finira plutôt à "+ f.ToString("HH:mm") +".\n";
                                    }

                                    if (j.DateHeureFinPerformance != jouerSurAModifier.DateHeureFinPerformance && j.DateHeureDebutPerformance != jouerSurAModifier.DateHeureDebutPerformance)
                                    {
                                        n = 1;
                                        message = message + "En ce qui concerne les horaires de passage: \n \n";
                                        var h = jouerSurAModifier.DateHeureDebutPerformance;
                                        var f = jouerSurAModifier.DateHeureFinPerformance;
                                        message = message + "Votre artiste commencera le: " + h.ToString("dd/MM/yyyy") + " sur la scène " + sc.NomScene + " , à " + h.ToString("HH:mm") + ".\n";
                                    }

                                    //Fin ajout message de modification des horaires
                                    if (festivNotif != null)
                                    {
                                        if (n == 1)
                                        {
                                            foreach (var f in festivNotif)
                                            {
                                                Modifications modif = new Modifications();
                                                modif.FestivalierId = f.Id;
                                                modif.Message = message;
                                                modif.Heure = System.DateTime.Now;
                                                modif.LuOuNon = false;
                                                DAOAPI.Instance.AjoutModificationAsync(modif).Wait();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ViewData["messageAttention"] = "Impossible de modifier l'artiste, sa nouvelle plage horaire dépasse sur une autre prestation.";
                                }
                            }
                            else
                            {
                                ViewData["messageAttention"] = "Impossible de modifier l'artiste, l'heure de début est supérieure à l'heure de fin";
                            }
                        }
                        else
                        {
                            ViewData["messageAttention"] = "Impossible d'effectuer l'action, certains champs sont vides.";
                        }
                        //on recharge les Artistes et les JouerSur pour que l'affichage prenne en compte les modifications apportées par
                        //les ajouts ou put  dans l'API
                        listeToutLesArtistes = _DAOAPI.Instance.GetArtistes().Result;
                        listeJouerSurs = _DAOAPI.Instance.GetJouerSurs().Result;
                        listeToutesLesScenes = _DAOAPI.Instance.GetScenes().Result;
                        listeScenes = (from scene in listeToutesLesScenes where scene.IdFestival == idFestivalDeOrgaConnecte select scene).ToList();

                    }
                    ViewData["listeArtistes"] = listeToutLesArtistes;
                    ViewData["listeJouerSurs"] = listeJouerSurs;
                    ViewData["listeScenes"] = listeScenes;
                    return View();
                }
                else
                {
                    ViewData["messageAttention"] = "Afin d'accéder à cette espace, il faut que votre festival soit accepté et crée au préalable.";
                    return RedirectToAction("MonFestival", "EspaceOrganisateur");
                }
            }
            else
            {
                return RedirectToAction("Deconnexion", "EspaceOrganisateur");
            }
        }

        public IActionResult InscriptionOrganisateur()
        {
            Organisateur organisateur = new Organisateur();

            if (Request.Method == "POST")
            {
                string Nom = Request.Form["Nom"];
                string Prenom = Request.Form["Prenom"];
                string Mail = Request.Form["mail"];
                string MotDePasse = Request.Form["MotDePasse"];

                organisateur.Nom = Nom;
                organisateur.Prenom = Prenom;
                organisateur.AdresseMail = Mail;
                organisateur.MotDePasse = MotDePasse;

                ViewBag.NomOrg = Nom;
                ViewBag.PrenomOrg = Prenom;
                ViewBag.MailOrg = Mail;
                ViewBag.MdpOrg = MotDePasse;

                Uri uri = _DAOAPI.Instance.AjoutOrganisateurAsync(organisateur).Result;

                if (uri == null)
                {
                    return View();
                }
                else
                {

                    ViewData["Message"] = "votre compte a bien été créer";
                    return View("Recapitulatif");
                }
            }
            else
            {
                return View();
            }
        }

        public IActionResult Ventes()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("organisateurConnecte")))
            {
                int idFestivalDeOrgaConnecte = JsonConvert.DeserializeObject<Organisateur>(HttpContext.Session.GetString("organisateurConnecte")).IdFestival;
                List<Billet> listeBillets = (from billet in _DAOAPI.Instance.GetBillets().Result where billet.IdFestival == idFestivalDeOrgaConnecte select billet).ToList<Billet>();

                Dictionary<DateTime, int> dataGraphique = new Dictionary<DateTime, int>();
                int totalDesVentes = listeBillets.Count();
                float montantTotalDesVentes = 0;


                foreach(Billet billet in listeBillets)
                {
                    if (!dataGraphique.ContainsKey(billet.DateAchat.Date))
                    {
                        dataGraphique.Add(billet.DateAchat.Date, 1);
                    }
                    else
                    {
                        dataGraphique[billet.DateAchat.Date]++;
                    }
                    montantTotalDesVentes += billet.Montant;
                }

                ViewData["dataGraphique"] = dataGraphique;
                ViewData["totalDesVentes"] = totalDesVentes;
                ViewData["montantTotalDesVentes"] = montantTotalDesVentes;

                return View();
            }
            else
            {
                return RedirectToAction("Deconnexion", "EspaceOrganisateur");
            }
        }

        public IActionResult GererLeFestival()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("organisateurConnecte")))
            {

                int idOrganisateur = JsonConvert.DeserializeObject<Organisateur>(HttpContext.Session.GetString("organisateurConnecte")).Id;

                Organisateur nouveauOrganisateur = DAOAPI.Instance.GetOrganisateurs(idOrganisateur).Result;

                int idfestival = nouveauOrganisateur.IdFestival;

                Festival NouveauFestival = new Festival();
                NouveauFestival = DAOAPI.Instance.GetFestivalAsync(idfestival).Result;


                if (NouveauFestival.PublieOuNon == false)
                {

                    NouveauFestival.PublieOuNon = true;
                    DAOAPI.Instance.PutFestivals(NouveauFestival.Id, NouveauFestival).Wait();

                }
                else
                {
                    NouveauFestival.PublieOuNon = false;
                    DAOAPI.Instance.PutFestivals(NouveauFestival.Id, NouveauFestival).Wait();
                }

                ViewData["festivalDeOrgaConnecte"] = DAOAPI.Instance.GetFestivalAsync(NouveauFestival.Id).Result;
                return RedirectToAction("MonFestival", "EspaceOrganisateur");
            }
            else
            {
                return RedirectToAction("Deconnexion", "EspaceOrganisateur");
            }

        }

        public IActionResult GererLesInscriptions()
        {
            int idOrganisateur = JsonConvert.DeserializeObject<Organisateur>(HttpContext.Session.GetString("organisateurConnecte")).Id;

            Organisateur nouveauOrganisateur = DAOAPI.Instance.GetOrganisateurs(idOrganisateur).Result;

            int idfestival = nouveauOrganisateur.IdFestival;

            Festival NouveauFestival = new Festival();
            NouveauFestival = DAOAPI.Instance.GetFestivalAsync(idfestival).Result;

            if (NouveauFestival.InscriptionsOuvertesOuNOn == false)
            {

                NouveauFestival.InscriptionsOuvertesOuNOn = true;
                DAOAPI.Instance.PutFestivals(NouveauFestival.Id, NouveauFestival).Wait();

            }
            else
            {
                NouveauFestival.InscriptionsOuvertesOuNOn = false;
                DAOAPI.Instance.PutFestivals(NouveauFestival.Id, NouveauFestival).Wait();
            }
            ViewData["festivalDeOrgaConnecte"] = DAOAPI.Instance.GetFestivalAsync(NouveauFestival.Id).Result;
            return RedirectToAction("MonFestival", "EspaceOrganisateur");
        }

        public IActionResult ValidationFestivalier()
        {

            int idfestival = JsonConvert.DeserializeObject<Organisateur>(HttpContext.Session.GetString("organisateurConnecte")).IdFestival;
            Festival festivall = DAOAPI.Instance.GetFestivalAsync(idfestival).Result;
            List<Billet> Billetsfest = new List<Billet>();
            List<Billet> ListeBillets = DAOAPI.Instance.GetBillets().Result;
            List<Festivalier> ListeFestivaliers = DAOAPI.Instance.GetFestivaliers().Result;
            Billetsfest = (from billet in ListeBillets where billet.IdFestival == idfestival select billet).ToList<Billet>();
            ViewData["Billet"] = Billetsfest;
            ViewData["Festivalier"] = ListeFestivaliers;
            return View("MonFestival/ValidationFestivalier");
        }


        public IActionResult RefuserFestivalier(int? Id)
        {
            
            Billet NouveauBillet = new Billet();
            NouveauBillet = DAOAPI.Instance.GetBilletAsync(Id).Result;

            if (NouveauBillet.Validation == 3)
            {

                NouveauBillet.Validation = 2;
                DAOAPI.Instance.PutBillet(NouveauBillet).Wait();

                DAOAPI.Instance.DeleteBilletAsync(NouveauBillet).Wait();

            }
            
            return Redirect("/EspaceOrganisateur/MonFestival/ValidationFestivalier");
          
        }


        public IActionResult ValiderFestivalier(int? Id)
        {


            Billet NouveauBillet = new Billet();
            NouveauBillet = DAOAPI.Instance.GetBilletAsync(Id).Result;

            if (NouveauBillet.Validation == 3)
            {

                NouveauBillet.Validation = 1;
                DAOAPI.Instance.PutBillet(NouveauBillet).Wait();

            }

            return Redirect("/EspaceOrganisateur/MonFestival/ValidationFestivalier");
        }


        public IActionResult Deconnexion()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
        
        public IActionResult ConsultationDuSite()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("organisateurConnecte")))
            {
                int idFestivalDeOrgaConnecte = JsonConvert.DeserializeObject<Organisateur>(HttpContext.Session.GetString("organisateurConnecte")).IdFestival;
                string Nom_Festival = DAOAPI.Instance.GetFestivalAsync(idFestivalDeOrgaConnecte).Result.Nom;
                ViewData["NomFestival"] = Nom_Festival;
                var consul = DAOAPI.Instance.GetConsultations().Result;
                List <Consultation> listeConsultation = (from cons in consul where cons.IdFestival == idFestivalDeOrgaConnecte select cons).ToList<Consultation>();

                Dictionary<DateTime, int> dataGraphique = new Dictionary<DateTime, int>();
                int totalDesConsultations = listeConsultation.Count();
                int nombreTotalDeVisite = 0;

       

                foreach (Consultation cons in listeConsultation)
                {
                    if (!dataGraphique.ContainsKey(cons.Date_enregistrement.Date))
                    {
                        dataGraphique.Add(cons.Date_enregistrement.Date, 1);
                    }
                    else
                    {
                        dataGraphique[cons.Date_enregistrement.Date]++;
                    }
                    nombreTotalDeVisite++;
                }

                ViewData["dataGraphique"] = dataGraphique;
                ViewData["totalDesconsultations"] = totalDesConsultations;
                ViewData["nombretotaldevisites"] = nombreTotalDeVisite;

                return View();
            }
            else
            {
                return RedirectToAction("Deconnexion", "EspaceOrganisateur");
            }
        }

        public IActionResult EvolutionDesSelections()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("organisateurConnecte")))
            {
                ICollection<Festival> festivals = DAOAPI.Instance.GetFestivals().Result;
                List<Artiste> artistes = DAOAPI.Instance.GetArtistes().Result;
                List<Selection> selections = DAOAPI.Instance.GetSelections().Result;
                ICollection<JouerSur> jouerSurs = DAOAPI.Instance.GetJouerSurs().Result;
                int idFestivalDeOrgaConnecte = JsonConvert.DeserializeObject<Organisateur>(HttpContext.Session.GetString("organisateurConnecte")).IdFestival;
                ICollection<Scene> scenes = DAOAPI.Instance.GetScenes().Result;

                List<int> Valeurs = new List<int>();
                List<string> Nomartistes = new List<string>();
                List<Festivalier> festivaliers = DAOAPI.Instance.GetFestivaliers().Result;
                List<Billet> billets = DAOAPI.Instance.GetBillets().Result;
                billets = (from b in billets where b.IdFestival == idFestivalDeOrgaConnecte select b).ToList<Billet>();
                List<Festivalier> festivaliersorga = new List<Festivalier>();
                List<Selection> selectionOrga = new List<Selection>();
                List<JouerSur> jouersurOrga = new List<JouerSur>();
                scenes = (from s in scenes where s.IdFestival == idFestivalDeOrgaConnecte select s).ToList<Scene>();
                
                foreach(var j in jouerSurs)
                {
                    foreach(var s in scenes)
                    {
                        if (j.IdScn == s.Id && !jouersurOrga.Contains(j)) 
                        {
                            jouersurOrga.Add(j);
                        }
                    }
                }

                    foreach (var fes in festivaliers)
                {
                    foreach(var b in billets)
                    {
                        if (b.IdFestivalier == fes.Id && !festivaliersorga.Contains(fes))
                        {
                            festivaliersorga.Add(fes);
                        }
                    }
                }
                foreach(var s in selections)
                {
                    foreach(var fes in festivaliersorga)
                    {
                        if (s.IdFestivalier == fes.Id && !selectionOrga.Contains(s))
                        {
                            selectionOrga.Add(s);
                        }
                    }
                }

                foreach (var a in artistes)
                {
                    foreach(var j in jouersurOrga)
                    {
                        if (a.Id == j.IdArtiste)
                        {
                            Festival f = (from fes in festivals where fes.Id == idFestivalDeOrgaConnecte select fes).FirstOrDefault();
                            int jour = (j.DateHeureDebutPerformance.Date - f.DateDeDebut.Date).Days+1;
                            if(!Nomartistes.Contains(a.Nom + " jour " + jour.ToString() + " " + j.DateHeureDebutPerformance.ToString("HH:mm")))
                            {
                                Nomartistes.Add(a.Nom + " jour " + jour.ToString() + " " + j.DateHeureDebutPerformance.ToString("HH:mm"));
                            }
                            
                        }
                    }
                }

                foreach(var n in Nomartistes)
                {
                    int nombre = 0;
                    foreach(var a in artistes)
                    {
                        foreach(var j in jouersurOrga)
                        {
                            foreach(var s in selectionOrga)
                            {
                                Festival f = (from fes in festivals where fes.Id == idFestivalDeOrgaConnecte select fes).FirstOrDefault();
                                int jour = (j.DateHeureDebutPerformance.Date - f.DateDeDebut.Date).Days+1;
                                string label = a.Nom + " jour " + jour.ToString() + " " + j.DateHeureDebutPerformance.ToString("HH:mm");
                                if (label==n && s.IdArtiste == a.Id)
                                {
                                    nombre++;
                                }
                            }
                        }
                    }
                    Valeurs.Add(nombre);
                }

                ViewData["Abscisse"] = Nomartistes;
                ViewData["Valeurs"] = Valeurs;
                return View();
            }
            else
            {
                return RedirectToAction("Deconnexion", "EspaceOrganisateur");
            }
            
        }



    }
}