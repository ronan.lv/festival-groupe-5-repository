﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using APIFestival.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SiteWebFestival.Controllers;
using SiteWebFestival.Models;

namespace Festival_Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public IActionResult ListeFestivals()
        {
            ICollection<Festival> listeFestivals = DAOAPI.Instance.GetFestivals().Result;
            return View(listeFestivals);
        }

        public IActionResult DescriptionFestival(int? id)
        {   
            if(id != null)
            {
                int Inscrits = DAOAPI.Instance.GetNombreInscrits(id).Result;
                TempData["Inscrits"] = Inscrits;
                TempData.Keep();
                ViewBag.Inscrits = TempData["Inscrits"];
                if (id == null)
                {
                    return null;
                }

                return View(DAOAPI.Instance.GetFestivalAsync(id).Result);
            }
            else
                return View("ErreurFormulaireIncorrect");
        }

        public IActionResult Inscription()
        {
            if (Request.Method.Equals("POST"))
            {
                if (!string.IsNullOrEmpty(Request.Form["prenom"]) && !string.IsNullOrEmpty(Request.Form["nom"])
                    && !string.IsNullOrEmpty(Request.Form["email"]) && !string.IsNullOrEmpty(Request.Form["dateDeNaissance"])
                    && !string.IsNullOrEmpty(Request.Form["genre"]) && !string.IsNullOrEmpty(Request.Form["telephone"])
                    && !string.IsNullOrEmpty(Request.Form["codePostal"]) && !string.IsNullOrEmpty(Request.Form["commune"])
                    && !string.IsNullOrEmpty(Request.Form["pays"]) && !string.IsNullOrEmpty(Request.Form["password1"])
                    && !string.IsNullOrEmpty(Request.Form["password2"]))
                {

                    if (Request.Form["password1"].Equals(Request.Form["password2"]))
                    {
                        Festivalier festivalier = new Festivalier();
                        festivalier.Prenom = Request.Form["prenom"];
                        festivalier.Nom = Request.Form["nom"];
                        festivalier.AdresseMail = Request.Form["email"];
                        festivalier.DateDeNaissance = DateTime.Parse(Request.Form["dateDeNaissance"]);
                        festivalier.Sexe = Request.Form["genre"];
                        festivalier.Telephone = Request.Form["telephone"];
                        festivalier.CodePostal = Int32.Parse(Request.Form["codePostal"]);
                        festivalier.Commune = Request.Form["commune"];
                        festivalier.Pays = Request.Form["pays"];
                        festivalier.MotDePasse = Request.Form["password1"];

                        _ = DAOAPI.Instance.AjoutFestivalierAsync(festivalier);

                        ViewData["messageReussiteOuNonInscription"] = "Inscription réussit !";

                      

                    }
                    else
                    {
                        ViewData["messageReussiteOuNonInscription"] = "Les deux mots de passe ne correspondent pas.";

                    }
                    return View();
                }
                else
                {
                    return View("ErreurFormulaireIncorrect");
                }
            }
            else
            {
                return View();
            }
        }

        public IActionResult Connexion()
        {
            if (Request.Method.Equals("POST"))
            {
                if (!string.IsNullOrEmpty(Request.Form["login"]) && !string.IsNullOrEmpty(Request.Form["password"]))
                {
                    Festivalier festivalier = new Festivalier();
                    festivalier.AdresseMail = Request.Form["login"];
                    festivalier.MotDePasse = Request.Form["password"];
                    if (DAOAPI.Instance.GetPresenceFestivalier(festivalier).Result)
                    {
                        List<Festivalier> listeFestivaliers = DAOAPI.Instance.GetFestivaliers().Result;
                        IEnumerable<Festivalier> requeteLINQ = from f in listeFestivaliers where f.AdresseMail.Equals(Request.Form["login"]) && f.MotDePasse.Equals(Request.Form["password"]) select f;
                        Festivalier festivalierConnecte = requeteLINQ.ElementAt(0);
                        HttpContext.Session.SetString("festivalierConnecte", JsonConvert.SerializeObject(festivalierConnecte));
                        List<Festival> listeFestivals = (from festival in DAOAPI.Instance.GetFestivals().Result join billet in DAOAPI.Instance.GetBillets().Result on festival.Id equals billet.IdFestival where billet.IdFestivalier == festivalierConnecte.Id select festival).ToList();
                        HttpContext.Session.SetString("listeFestivals", JsonConvert.SerializeObject(listeFestivals));

                        return RedirectToAction("Index", "EspaceFestivalier");
                    }
                    else
                    {
                        ViewData["messageReussiteOuNonConnexion"] = "Nom d'utilisateur ou mot de passe incorrect";
                        return View();
                    }
                    
                }
                else
                {
                    return View("ErreurFormulaireIncorrect");
                }
            }
            else
            {
                return View();
            }
        }

        public IActionResult InscriptionOrganisateur()
        {
            if (Request.Method.Equals("POST"))
            {
                if (!string.IsNullOrEmpty(Request.Form["prenom"]) && !string.IsNullOrEmpty(Request.Form["nom"])
                    && !string.IsNullOrEmpty(Request.Form["email"]) && !string.IsNullOrEmpty(Request.Form["telephone"])
                    && !string.IsNullOrEmpty(Request.Form["password1"]) && !string.IsNullOrEmpty(Request.Form["password2"]))
                {

                    if (Request.Form["password1"].Equals(Request.Form["password2"]))
                    {
                        Organisateur organisateur = new Organisateur();
                        organisateur.Prenom = Request.Form["prenom"];
                        organisateur.Nom = Request.Form["nom"];
                        organisateur.AdresseMail = Request.Form["email"];
                        organisateur.NumeroTelephone = Request.Form["telephone"];
                        organisateur.MotDePasse = Request.Form["password1"];
                        organisateur.IdFestival = -1; //Par défault, l'id de son festival est à -1, signifiant ainsi que cet organisateur n'est relié à aucun festival

                        _ = DAOAPI.Instance.AjoutOrganisateurAsync(organisateur);

                        ViewData["messageReussiteOuNonInscription"] = "Inscription réussit !";
                    }
                    else
                    {
                        ViewData["messageReussiteOuNonInscription"] = "Les deux mots de passe ne correspondent pas.";

                    }
                    return View();
                }
                else
                {
                    return View("ErreurFormulaireIncorrect");
                }
            }
            else
            {
                return View();
            }
        }

        public IActionResult ConnexionOrganisateur()
        {
            if (Request.Method.Equals("POST"))
            {
                if (!string.IsNullOrEmpty(Request.Form["login"]) && !string.IsNullOrEmpty(Request.Form["password"]))
                {
                    Organisateur organisateur = new Organisateur();
                    organisateur.AdresseMail = Request.Form["login"];
                    organisateur.MotDePasse = Request.Form["password"];
                    if (DAOAPI.Instance.GetPresenceOrganisateur(organisateur).Result)
                    {
                        List<Organisateur> listeOrganisateurs = DAOAPI.Instance.GetOrganisateurs().Result;
                        IEnumerable<Organisateur> requeteLINQ = from o in listeOrganisateurs where o.AdresseMail.Equals(Request.Form["login"]) && o.MotDePasse.Equals(Request.Form["password"]) select o;
                        Organisateur organisateurConnecte = requeteLINQ.ElementAt(0);
                        HttpContext.Session.SetString("organisateurConnecte", JsonConvert.SerializeObject(organisateurConnecte));
                        return RedirectToAction("Index", "EspaceOrganisateur");
                    }
                    else
                    {
                        ViewData["messageReussiteOuNonConnexion"] = "Nom d'utilisateur ou mot de passe incorrect";
                        return View();
                    }
                }
                else
                {
                    return View("ErreurFormulaireIncorrect");
                }
            }
            else
            {
                return View();
            }
        }

        public IActionResult ConsulterProgrammation(int id)
        {

            //  JouerSur jouersur = DAOAPI.Instance.GetProgrammeFestival(5).Result;
            // Artiste artiste = DAOAPI.Instance.GetArtiste(jouersur.IdArtiste).Result;
            //Scene scene = DAOAPI.Instance.GetScene(jouersur.IdScn).Result;

            // ViewData["ConsulterProgrammation"] = jouersur;
            // ViewData["artiste"] = artiste;
            // ViewData["scene"] = scene;

            return View(DAOAPI.Instance.GetFestivalAsync(id).Result) ;
        }
       

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
