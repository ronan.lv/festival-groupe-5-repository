﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using APIFestival.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace SiteWebFestival.Controllers
{

    public class _DAOAPI
    {
        private readonly HttpClient client = new HttpClient();

        public _DAOAPI()
        {
            client.BaseAddress = new Uri("http://localhost:54566");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
        private static readonly object padlock = new object();
        private static _DAOAPI instance = null;

        public static _DAOAPI Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new _DAOAPI();
                    }
                    return instance;
                }
            }
        }

        public async Task<List<Artiste>> GetArtistes()
        {
            List<Artiste> ListeArtistes = new List<Artiste>();
            HttpResponseMessage response = client.GetAsync("api/Artistes").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeArtistes = JsonConvert.DeserializeObject<List<Artiste>>(resp);
            }
            return ListeArtistes;
        }
        public async Task<Artiste> GetArtiste(int? Id)
        {
            HttpResponseMessage response = client.GetAsync("api/Artistes/" + Id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                var value = JsonConvert.DeserializeObject<Artiste>(resp);
                return value;
            }
            return null;
        }        public async Task<List<Modifications>> GetModificationsFestivalier(int Id)
        {
            HttpResponseMessage response = client.GetAsync("api/Modifications/Festivalier/" + Id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                var value = JsonConvert.DeserializeObject<List<Modifications>>(resp);
                return value;
            }
            return null;
        }        public async Task<Uri> PutModifications(Modifications m)
        {
            try
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("api/Modifications/" + m.Id, m);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception e)
            {
                Debug.WriteLine("------------------ \n" + e.ToString() + " \n---------------------");
            }
            return null;
        }



        public async Task<List<Billet>> GetBillets()
        {
            List<Billet> ListeBillets = new List<Billet>();
            HttpResponseMessage response = client.GetAsync("api/Billets").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeBillets = JsonConvert.DeserializeObject<List<Billet>>(resp);
            }
            return ListeBillets;
        }        public async Task<List<Selection>> GetSelections()
        {
            List<Selection> selections = new List<Selection>();
            HttpResponseMessage response = client.GetAsync("api/Selections").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                selections = JsonConvert.DeserializeObject<List<Selection>>(resp);
            }
            return selections;
        }      /*  public async Task<List<JouerSur>> GetJouerSurs()
        {
            List<JouerSur> JouerSurs = new List<JouerSur>();
            HttpResponseMessage response = client.GetAsync("api/JouerSurs").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                JouerSurs = JsonConvert.DeserializeObject<List<JouerSur>>(resp);
            }
            return JouerSurs;
        }*/
        public async Task<List<Consultation>> GetConsultations()
        {
            List<Consultation> ListeConsultations = new List<Consultation>();
            HttpResponseMessage response = client.GetAsync("api/Consultations").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeConsultations = JsonConvert.DeserializeObject<List<Consultation>>(resp);
            }
            return ListeConsultations;
        }        public async Task<Uri> AjoutConsultationAsync(Consultation consultation)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Consultations", consultation);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> PutFestivals(int? idFestivalAModifier, Festival festival)
        {
            try
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("api/festivals/" + idFestivalAModifier, festival);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception e)
            {
                Debug.WriteLine("------------------ \n" + e.ToString() + " \n---------------------");
            }
            return null;
        }


        public async Task<Uri> PutRelationAmicale(RelationAmicale relationAmicale)
        {
            try
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("api/relationAmicales/" + relationAmicale.Id, relationAmicale);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception e)
            {
                Debug.WriteLine("------------------ \n" + e.ToString() + " \n---------------------");
            }
            return null;
        }

        public async Task<int> GetNombreInscrits(int? Id)
        {
            int n = 0;
            HttpResponseMessage response = client.GetAsync("api/Billets/Festival/" + Id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                n = JsonConvert.DeserializeObject<IEnumerable<Billet>>(resp).Count<Billet>();
            }
            return n;
        }

        public async Task<ICollection<Festival>> GetFestivals()
        {
            ICollection<Festival> ListeFestivals = new List<Festival>();
            HttpResponseMessage response = client.GetAsync("api/Festivals").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeFestivals = JsonConvert.DeserializeObject<List<Festival>>(resp);

            }

            return ListeFestivals;
        }

        public async Task<List<Festivalier>> GetFestivaliers()
        {
            List<Festivalier> ListeFestivaliers = new List<Festivalier>();
            HttpResponseMessage response = client.GetAsync("api/Festivaliers").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeFestivaliers = JsonConvert.DeserializeObject<List<Festivalier>>(resp);

            }

            return ListeFestivaliers;
        }
        public async Task<Festivalier> GetFestivaliersByMail(String email)
        {
            Festivalier festivalier = null;
            HttpResponseMessage response = client.GetAsync("api/Festivaliers/ByMail" + email).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                festivalier = JsonConvert.DeserializeObject<Festivalier>(resp);
            }
            return festivalier;
        }


        public async Task<List<RelationAmicale>> GetRelationAmicales()
        {
            List<RelationAmicale> ListeRelationAmicales = new List<RelationAmicale>();
            HttpResponseMessage response = client.GetAsync("api/RelationAmicales").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeRelationAmicales = JsonConvert.DeserializeObject<List<RelationAmicale>>(resp);

            }

            return ListeRelationAmicales;
        }



        public async Task<List<Organisateur>> GetOrganisateurs()
        {
            List<Organisateur> ListeOrganisateurs = new List<Organisateur>();
            HttpResponseMessage response = client.GetAsync("api/Organisateurs").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeOrganisateurs = JsonConvert.DeserializeObject<List<Organisateur>>(resp);

            }

            return ListeOrganisateurs;
        }

        public async Task<Organisateur> GetOrganisateurs(int id)
        {
            Organisateur organisateur = null;
            HttpResponseMessage response = client.GetAsync("api/Organisateurs/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                organisateur = JsonConvert.DeserializeObject<Organisateur>(resp);
            }
            return organisateur;
        }




        public async Task<Festival> GetFestivalAsync(int? id)
        {
            Festival festival = null;
            HttpResponseMessage response = client.GetAsync("api/festivals/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                festival = JsonConvert.DeserializeObject<Festival>(resp);
            }
            return festival;
        }



        public async Task<ICollection<Scene>> GetScenes()
        {
            ICollection<Scene> ListeScenes = new List<Scene>();
            HttpResponseMessage response = client.GetAsync("api/Scenes").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeScenes = JsonConvert.DeserializeObject<List<Scene>>(resp);

            }

            return ListeScenes;
        }        public async Task<ICollection<Festivalier>> GetFestivalierSelection(Artiste a)
        {
            ICollection<Festivalier> ListeFestivaliers = new List<Festivalier>();
            HttpResponseMessage response = client.GetAsync("api/Selections/Festivaliers/"+a.Id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeFestivaliers = JsonConvert.DeserializeObject<List<Festivalier>>(resp);

            }

            return ListeFestivaliers;
        }

        public async Task<Scene> GetScene(int? Id)
        {
            HttpResponseMessage response = client.GetAsync("api/Scenes/" + Id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                var value = JsonConvert.DeserializeObject<Scene>(resp);
                return value;
            }
            return null;
        }









        public async Task<ICollection<Hebergement>> GetHebergements()
        {
            ICollection<Hebergement> ListeHebergements = new List<Hebergement>();
            HttpResponseMessage response = client.GetAsync("api/Hebergements").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeHebergements = JsonConvert.DeserializeObject<List<Hebergement>>(resp);

            }

            return ListeHebergements;
        }

        public async Task<ICollection<JouerSur>> GetJouerSurs()
        {
            ICollection<JouerSur> ListeJouerSurs = new List<JouerSur>();
            HttpResponseMessage response = client.GetAsync("api/JouerSurs").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeJouerSurs = JsonConvert.DeserializeObject<List<JouerSur>>(resp);

            }

            return ListeJouerSurs;
        }

        public async Task<ICollection<ParticipeA>> GetParticipeAs()
        {
            ICollection<ParticipeA> ListeParticipeAs = new List<ParticipeA>();
            HttpResponseMessage response = client.GetAsync("api/ParticipeAs").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeParticipeAs = JsonConvert.DeserializeObject<List<ParticipeA>>(resp);

            }

            return ListeParticipeAs;
        }

        public async Task<Uri> AjoutArtisteAsync(Artiste artiste)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Artistes", artiste);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        public async Task<Uri> AjoutRelationAmicaleAsync(RelationAmicale relationAmicale)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/RelationAmicales", relationAmicale);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> AjoutFestivalAsync(Festival festival)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Festivals", festival);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> AjoutOrganisateurAsync(Organisateur organisateur)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Organisateurs", organisateur);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> AjoutFestivalierAsync(Festivalier festivalier)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Festivaliers", festivalier);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> AjoutModificationAsync(Modifications m)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Modifications", m);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> AjoutSceneAsync(Scene scene)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Scenes", scene);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> AjoutJouerSurAsync(JouerSur jouerSur)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/JouerSurs", jouerSur);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> AjoutParticipationAsync(ParticipeA participeA)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/ParticipeAs", participeA);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> AjoutHebergementAsync(Hebergement hebergement)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Hebergements", hebergement);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> AjoutBilletAsync(Billet billet)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Billets", billet);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }


        public async Task<bool> GetPresenceOrganisateur(Organisateur o)
        {
            HttpResponseMessage response = client.GetAsync("api/Organisateurs/" + o.AdresseMail + "/" + o.MotDePasse).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                var value = JsonConvert.DeserializeObject<bool>(resp);
                return value;
            }
            else
            {
                return false;
            }

        }

        public async Task<bool> GetPresenceFestivalier(Festivalier f)
        {
            HttpResponseMessage response = client.GetAsync("api/Festivaliers/" + f.AdresseMail + "/" + f.MotDePasse).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                var value = JsonConvert.DeserializeObject<bool>(resp);
                return value;
            }
            else
            {
                return false;
            }

        }

        public async Task<JouerSur> GetProgrammeFestival(int? Id)
        {
            HttpResponseMessage response = client.GetAsync("api/JouerSurs/" + Id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                var value = JsonConvert.DeserializeObject<JouerSur>(resp);
                return value;
            }
            return null;
        }

        public async Task<Festival> GetSonFestival(int? id)
        {
            HttpResponseMessage response = client.GetAsync("api/Festivals/Organisateur/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                var value = JsonConvert.DeserializeObject<Festival>(resp);
                return value;
            }
            return null;
        }

        public async Task<int> GetFestivalValide(string Nom, string Url, string Description)
        {
            HttpResponseMessage response = client.GetAsync("api/Festivals/" + Nom + "/" + Url + "/" + Description).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                var value = JsonConvert.DeserializeObject<int>(resp);
                return value;
            }
            return 0;
        }

        public async Task<int> GetIdOrganisateur(string mail, string mdp)
        {
            HttpResponseMessage response = client.GetAsync("api/Organisateurs/" + mail + "/" + mdp).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                var value = JsonConvert.DeserializeObject<int>(resp);
                return value;
            }
            return 0;
        }

        public async Task<Uri> DeleteArtisteAsync(Artiste artiste)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync("api/artistes/" + artiste.Id);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> DeleteJouerSurAsync(JouerSur jouerSur)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync("api/jouerSurs/" + jouerSur.Id);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> PutArtiste(Artiste artiste)
        {
            try
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("api/artistes/" + artiste.Id, artiste);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception e)
            {
                Debug.WriteLine("------------------ \n" + e.ToString() + " \n---------------------");
            }
            return null;
        }

        public async Task<Uri> PutJouerSur(JouerSur jouerSur)
        {
            try
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("api/jouersurs/" + jouerSur.Id, jouerSur);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception e)
            {
                Debug.WriteLine("------------------ \n" + e.ToString() + " \n---------------------");
            }
            return null;
        }

        public async Task<Uri> PutBillet(Billet billet)
        {
            try
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("api/billets/" + billet.Id, billet);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception e)
            {
                Debug.WriteLine("------------------ \n" + e.ToString() + " \n---------------------");
            }
            return null;
        }        //recuperer le billet à partir de l'Id        public async Task<Billet> GetBilletAsync(int? id)
        {
            Billet billet = null;
            HttpResponseMessage response = client.GetAsync("api/Billets/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                billet = JsonConvert.DeserializeObject<Billet>(resp);
            }
            return billet;
        }
        //supprimer un billet
        public async Task<Uri> DeleteBilletAsync(Billet billet)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync("api/billets/" + billet.Id);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }


        public async Task<List<Selection>> GetSelections()
        {
            List<Selection> listeSelections = new List<Selection>();
            HttpResponseMessage response = client.GetAsync("api/Selections").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                listeSelections = JsonConvert.DeserializeObject<List<Selection>>(resp);

            }
            return listeSelections;
        }
        public async Task<Uri> AjoutSelectionAsync(Selection selection)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Selections", selection);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> DeleteSelectionAsync(Selection selection)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync("api/selections/" + selection.Id);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        public async Task<ICollection<JouerSur>> GetFestivalierByFestivalId(int ArtisteID)
        {
            ICollection<JouerSur> jouerSur = null;
            HttpResponseMessage response = client.GetAsync("api/JouerSurs/ArtisteID/" + ArtisteID).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                jouerSur = JsonConvert.DeserializeObject<ICollection<JouerSur>>(resp);
            }
            return jouerSur;
        }

        public async Task<ICollection<Selection>> GetSelectionByFestivalierId(int IdFestivalier)
        {
            ICollection<Selection> selection = null;
            HttpResponseMessage response = client.GetAsync("api/Selections/FestivalierID/" + IdFestivalier).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                selection = JsonConvert.DeserializeObject<ICollection<Selection>>(resp);
            }
            return selection;
        }


    }
    }

