﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using APIFestival.Models;

namespace APIFestival.Data
{
    public class APIFestivalContext : DbContext
    {
        public APIFestivalContext (DbContextOptions<APIFestivalContext> options)
            : base(options)
        {
        }

        public DbSet<APIFestival.Models.Artiste> Artiste { get; set; }

        public DbSet<APIFestival.Models.Festival> Festival { get; set; }

        public DbSet<APIFestival.Models.Festivalier> Festivalier { get; set; }

        public DbSet<APIFestival.Models.Gestionnaire> Gestionnaire { get; set; }

        public DbSet<APIFestival.Models.Hebergement> Hebergement { get; set; }

        public DbSet<APIFestival.Models.JouerSur> JouerSur { get; set; }

        public DbSet<APIFestival.Models.Organisateur> Organisateur { get; set; }

        public DbSet<APIFestival.Models.ParticipeA> ParticipeA { get; set; }

        public DbSet<APIFestival.Models.Scene> Scène { get; set; }

        public DbSet<APIFestival.Models.Billet> Billet { get; set; }

        public DbSet<APIFestival.Models.RelationAmicale> RelationAmicale { get; set; }

        public DbSet<APIFestival.Models.Modifications> Modifications { get; set; }

        public DbSet<APIFestival.Models.Consultation> Consultation { get; set; }

        public DbSet<APIFestival.Models.Selection> Selection { get; set; }
    }
}
