﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIFestival.Data;
using APIFestival.Models;

namespace APIFestival.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FestivalsController : ControllerBase
    {
        private readonly APIFestivalContext _context;

        public FestivalsController(APIFestivalContext context)
        {
            _context = context;
        }

        // GET: api/Festivals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Festival>>> GetFestival()
        {
            return await _context.Festival.Include(c => c.jouerSurs).ToListAsync();
        }

        // GET: api/Festivals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Festival>> GetFestival(int id)
        {
            var festival = await _context.Festival.Include(c => c.jouerSurs).FirstOrDefaultAsync(c => c.Id == id);

            if (festival == null)
            {
                return NotFound();
            }

            return festival;
        }

        // PUT: api/Festivals/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFestival(int id, Festival festival)
        {
            if (id != festival.Id)
            {
                return BadRequest();
            }

            _context.Entry(festival).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FestivalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Festivals
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Festival>> PostFestival(Festival festival)
        {
            _context.Festival.Add(festival);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFestival", new { id = festival.Id }, festival);
        }

        // DELETE: api/Festivals/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Festival>> DeleteFestival(int id)
        {
            var festival = await _context.Festival.FindAsync(id);
            if (festival == null)
            {
                return NotFound();
            }

            _context.Festival.Remove(festival);
            await _context.SaveChangesAsync();

            return festival;
        }

        private bool FestivalExists(int id)
        {
            return _context.Festival.Any(e => e.Id == id);
        }

        /*A partir d'ici commencent les modifications apportées à l'API */

        /* Commenté car fait planter l'API sinon (plusieurs points d'entrées)
        // GET: api/Festivals/Organisateur/Id
        [HttpGet("{id}")]
        public async Task<ActionResult<Festival>> GetSonFestival(int id)
        {
            var festival = await _context.Festival.FirstOrDefaultAsync(f => f.IdOrganisateur == id);
            if (festival == null)
            {
                return NotFound();
            }
            return festival;
        }
        */

        // GET: api/Festivals/Nom/Url/Description
        [HttpGet("{Nom}/{Url}/{Description}")]
        public async Task<ActionResult<int>> GetFestivalValide(string Nom, string Url = null, string Description = null)
        {
            var festival = await _context.Festival.FirstOrDefaultAsync(f => (f.Nom == Nom && f.UrlLogo == Url && f.Description == Description));
            if (festival == null && Url!=null)
            {
                festival = await _context.Festival.FirstOrDefaultAsync(f => f.Nom == Nom);
                if (festival == null)
                {
                    return NotFound();
                }
                else
                {
                    return festival.Id;
                }

            }
            return festival.Id;
        }


        //GET: api/Festivals/Festivaliers/id
        [HttpGet("{Festivals}/{id}")]
        public async Task<List<Festivalier>> GetNombreFestivalierP(int id)
        {
            var Billets = await _context.Billet.Where(b => b.IdFestival == id).ToListAsync();

            List<Festivalier> IntBillets = new List<Festivalier>();

            foreach (Billet b in Billets)
            {
                IntBillets.Add(await _context.Festivalier.FindAsync(b.IdFestivalier));
            }
            //Where(f => IntBillets.Contains(f.Id))
            //var festivaliers = await _context.Festivalier.ToListAsync();

            if (Billets == null)
            {
                return null;
            }
            return IntBillets;
        }
        /*
        [HttpGet("Nom/{nom}")]
        public async Task<ActionResult<Festival>> GetFestival(string nom)
        {
            var festival = await _context.Festival.FirstOrDefaultAsync(u => u.Nom.Equals(nom));

            if (festival == null)
            {
                return NotFound();
            }

            return festival;
        }
        */
    }
}
