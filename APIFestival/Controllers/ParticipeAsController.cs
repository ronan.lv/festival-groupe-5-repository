﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIFestival.Data;
using APIFestival.Models;

namespace APIFestival.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParticipeAsController : ControllerBase
    {
        private readonly APIFestivalContext _context;

        public ParticipeAsController(APIFestivalContext context)
        {
            _context = context;
        }

        // GET: api/ParticipeAs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ParticipeA>>> GetParticipeA()
        {
            return await _context.ParticipeA.ToListAsync();
        }

        // GET: api/ParticipeAs/Inscrits/Id
        [HttpGet("{Id}")]
        public async Task<ActionResult<int>> GetNombreInscrits(int Id)
        {
            var ListeFestivaliers = await _context.Festivalier.Where(f => (f.Id == Id)).ToListAsync();
            if(ListeFestivaliers == null)
            {
                return 0;
            }
            return ListeFestivaliers.Count();
        }

        // GET: api/ParticipeAs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ParticipeA>> GetParticipeA(int id)
        {
            var participeA = await _context.ParticipeA.FindAsync(id);

            if (participeA == null)
            {
                return NotFound();
            }

            return participeA;
        }

        // PUT: api/ParticipeAs/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParticipeA(int id, ParticipeA participeA)
        {
            if (id != participeA.Id)
            {
                return BadRequest();
            }

            _context.Entry(participeA).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParticipeAExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ParticipeAs
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ParticipeA>> PostParticipeA(ParticipeA participeA)
        {
            _context.ParticipeA.Add(participeA);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetParticipeA", new { id = participeA.Id }, participeA);
        }

        // DELETE: api/ParticipeAs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ParticipeA>> DeleteParticipeA(int id)
        {
            var participeA = await _context.ParticipeA.FindAsync(id);
            if (participeA == null)
            {
                return NotFound();
            }

            _context.ParticipeA.Remove(participeA);
            await _context.SaveChangesAsync();

            return participeA;
        }

        private bool ParticipeAExists(int id)
        {
            return _context.ParticipeA.Any(e => e.Id == id);
        }

    }
}
