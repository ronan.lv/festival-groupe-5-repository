﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIFestival.Data;
using APIFestival.Models;

namespace APIFestival.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JouerSursController : ControllerBase
    {
        private readonly APIFestivalContext _context;

        public JouerSursController(APIFestivalContext context)
        {
            _context = context;
        }

        // GET: api/JouerSurs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<JouerSur>>> GetJouerSur()
        {
            return await _context.JouerSur.ToListAsync();
        }

        // GET: api/JouerSurs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<JouerSur>> GetJouerSur(int id)
        {
            var jouerSur = await _context.JouerSur.FindAsync(id);

            if (jouerSur == null)
            {
                return NotFound();
            }

            return jouerSur;
        }

        // PUT: api/JouerSurs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutJouerSur(int id, JouerSur jouerSur)
        {
            if (id != jouerSur.Id)
            {
                return BadRequest();
            }

            _context.Entry(jouerSur).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JouerSurExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/JouerSurs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<JouerSur>> PostJouerSur(JouerSur jouerSur)
        {
            _context.JouerSur.Add(jouerSur);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetJouerSur", new { id = jouerSur.Id }, jouerSur);
        }

        // DELETE: api/JouerSurs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<JouerSur>> DeleteJouerSur(int id)
        {
            var jouerSur = await _context.JouerSur.FindAsync(id);
            if (jouerSur == null)
            {
                return NotFound();
            }

            _context.JouerSur.Remove(jouerSur);
            await _context.SaveChangesAsync();

            return jouerSur;
        }

        private bool JouerSurExists(int id)
        {
            return _context.JouerSur.Any(e => e.Id == id);
        }

        // GET: api/JouerSur/ArtisteId/1
        [HttpGet("ArtisteID/{ArtisteID}")]
        public async Task<ActionResult<IEnumerable<JouerSur>>> GetFestivalierByFestivalId(int ArtisteID)
        {
            var jouersur = _context.JouerSur.Where(f => f.IdArtiste == ArtisteID);

            if (jouersur == null)
            {
                return NotFound();
            }

            return await jouersur.ToListAsync();

        }
    }
}
