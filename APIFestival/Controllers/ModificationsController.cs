﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIFestival.Data;
using APIFestival.Models;

namespace APIFestival.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModificationsController : ControllerBase
    {
        private readonly APIFestivalContext _context;

        public ModificationsController(APIFestivalContext context)
        {
            _context = context;
        }

        // GET: api/Modifications
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Modifications>>> GetModifications()
        {
            return await _context.Modifications.ToListAsync();
        }

        // GET: api/Modifications/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Modifications>> GetModifications(int id)
        {
            var modifications = await _context.Modifications.FindAsync(id);

            if (modifications == null)
            {
                return NotFound();
            }

            return modifications;
        }

        // PUT: api/Modifications/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutModifications(int id, Modifications modifications)
        {
            if (id != modifications.Id)
            {
                return BadRequest();
            }

            _context.Entry(modifications).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModificationsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Modifications
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Modifications>> PostModifications(Modifications modifications)
        {
            _context.Modifications.Add(modifications);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetModifications", new { id = modifications.Id }, modifications);
        }

        // DELETE: api/Modifications/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Modifications>> DeleteModifications(int id)
        {
            var modifications = await _context.Modifications.FindAsync(id);
            if (modifications == null)
            {
                return NotFound();
            }

            _context.Modifications.Remove(modifications);
            await _context.SaveChangesAsync();

            return modifications;
        }

        private bool ModificationsExists(int id)
        {
            return _context.Modifications.Any(e => e.Id == id);
        }

        //Modifications Controleur

        // GET: api/Modifications/Festivalier/id
        [HttpGet("{Festivalier}/{id}")]
        public async Task<ActionResult<List<Modifications>>> GetModificationsFestivalier(int id)
        {
            var modifications = await _context.Modifications.Where(m => m.FestivalierId == id).ToListAsync();

            if (modifications == null)
            {
                return NotFound();
            }

            return modifications;
        }
    }
}
