﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIFestival.Data;
using APIFestival.Models;

namespace APIFestival.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RelationAmicalesController : ControllerBase
    {
        private readonly APIFestivalContext _context;

        public RelationAmicalesController(APIFestivalContext context)
        {
            _context = context;
        }

        // GET: api/RelationAmicales
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RelationAmicale>>> GetRelationAmicale()
        {
            return await _context.RelationAmicale.ToListAsync();
        }

        // GET: api/RelationAmicales/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RelationAmicale>> GetRelationAmicale(int id)
        {
            var relationAmicale = await _context.RelationAmicale.FindAsync(id);

            if (relationAmicale == null)
            {
                return NotFound();
            }

            return relationAmicale;
        }

        // PUT: api/RelationAmicales/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRelationAmicale(int id, RelationAmicale relationAmicale)
        {
            if (id != relationAmicale.Id)
            {
                return BadRequest();
            }

            _context.Entry(relationAmicale).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RelationAmicaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RelationAmicales
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<RelationAmicale>> PostRelationAmicale(RelationAmicale relationAmicale)
        {
            _context.RelationAmicale.Add(relationAmicale);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRelationAmicale", new { id = relationAmicale.Id }, relationAmicale);
        }

        // DELETE: api/RelationAmicales/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RelationAmicale>> DeleteRelationAmicale(int id)
        {
            var relationAmicale = await _context.RelationAmicale.FindAsync(id);
            if (relationAmicale == null)
            {
                return NotFound();
            }

            _context.RelationAmicale.Remove(relationAmicale);
            await _context.SaveChangesAsync();

            return relationAmicale;
        }

        private bool RelationAmicaleExists(int id)
        {
            return _context.RelationAmicale.Any(e => e.Id == id);
        }
    }
}
