﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIFestival.Data;
using APIFestival.Models;

namespace APIFestival.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SelectionsController : ControllerBase
    {
        private readonly APIFestivalContext _context;

        public SelectionsController(APIFestivalContext context)
        {
            _context = context;
        }

        // GET: api/Selections
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Selection>>> GetSelection()
        {
            return await _context.Selection.ToListAsync();
        }

        // GET: api/Selections/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Selection>> GetSelection(int id)
        {
            var selection = await _context.Selection.FindAsync(id);

            if (selection == null)
            {
                return NotFound();
            }

            return selection;
        }

        // PUT: api/Selections/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSelection(int id, Selection selection)
        {
            if (id != selection.Id)
            {
                return BadRequest();
            }

            _context.Entry(selection).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SelectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Selections
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Selection>> PostSelection(Selection selection)
        {
            _context.Selection.Add(selection);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSelection", new { id = selection.Id }, selection);
        }

        // DELETE: api/Selections/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Selection>> DeleteSelection(int id)
        {
            var selection = await _context.Selection.FindAsync(id);
            if (selection == null)
            {
                return NotFound();
            }

            _context.Selection.Remove(selection);
            await _context.SaveChangesAsync();

            return selection;
        }

        private bool SelectionExists(int id)
        {
            return _context.Selection.Any(e => e.Id == id);
        }

        //Modifications Contrôleur
        // GET: api/Selections/Festivaliers/id
        [HttpGet("{Festivaliers}/{id}")]
        public async Task<ActionResult<List<Festivalier>>> GetSelectionFestivalier(int id)
        {
            var selections = await _context.Selection.Where(s => s.IdArtiste == id).ToListAsync();
            var festivaliers = await _context.Festivalier.ToListAsync();
            List<Festivalier> fest = new List<Festivalier>();
            foreach (var f in festivaliers)
            {
                foreach (var s in selections)
                {
                    if (f.Id == s.IdFestivalier)
                    {
                        if (!fest.Contains(f))
                        {
                            fest.Add(f);
                        }
                    }
                }
            }

            if (fest == null)
            {
                return NotFound();
            }

            return fest;
        }
    }
}
