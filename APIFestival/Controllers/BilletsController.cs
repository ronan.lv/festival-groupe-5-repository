﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIFestival.Data;
using APIFestival.Models;

namespace APIFestival.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BilletsController : ControllerBase
    {
        private readonly APIFestivalContext _context;

        public BilletsController(APIFestivalContext context)
        {
            _context = context;
        }

        // GET: api/Billets
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Billet>>> GetBillet()
        {
            return await _context.Billet.ToListAsync();
        }

        // GET: api/Billets/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Billet>> GetBillet(int id)
        {
            var billet = await _context.Billet.FindAsync(id);

            if (billet == null)
            {
                return NotFound();
            }

            return billet;
        }

        // PUT: api/Billets/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBillet(int id, Billet billet)
        {
            if (id != billet.Id)
            {
                return BadRequest();
            }

            _context.Entry(billet).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BilletExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Billets
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Billet>> PostBillet(Billet billet)
        {
            _context.Billet.Add(billet);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBillet", new { id = billet.Id }, billet);
        }

        // DELETE: api/Billets/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Billet>> DeleteBillet(int id)
        {
            var billet = await _context.Billet.FindAsync(id);
            if (billet == null)
            {
                return NotFound();
            }

            _context.Billet.Remove(billet);
            await _context.SaveChangesAsync();

            return billet;
        }

        private bool BilletExists(int id)
        {
            return _context.Billet.Any(e => e.Id == id);
        }

        // GET: api/Billets/Festival/Id
        [HttpGet("{Festival}/{Id}")]
        public async Task<IEnumerable<Billet>> GetBilletsFestival(int Id)
        {
            var billets = await _context.Billet.Where(b => b.IdFestival == Id).ToListAsync();

            if (billets == null)
            {
                return null;
            }

            return billets;
        }
    }
}
