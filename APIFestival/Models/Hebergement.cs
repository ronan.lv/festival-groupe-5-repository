﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIFestival.Models
{
    public class Hebergement
    {
        public int Id { get; set; }
        [Required]
        public string Localisation { get; set; }
        public string Modalités { get; set; }

        [ForeignKey("FK_Festival")]
        public int IdFestival { get; set; }
    }
}
