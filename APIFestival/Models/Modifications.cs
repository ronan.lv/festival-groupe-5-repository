﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIFestival.Models
{
    public class Modifications
    {
        public int Id { get; set; }
        [Required]

        public int FestivalierId { get; set; }
        [ForeignKey("FK_Festivalier")]
        public string Message { get; set; }
        public DateTime Heure { get; set; }
        public Boolean LuOuNon { get; set; }
    }
}
