﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIFestival.Models
{
    public class RelationAmicale

    {   
        public int Id { get; set; }

        [ForeignKey("FK_Festivalier")]
        public int IdFestivalier { get; set; }

        [ForeignKey("FK_Festivalier")]
        public int IdAmi { get; set; }

        public DateTime dateAjoutAmi { get; set; }
        public int Statut { get; set; }

    }
}
