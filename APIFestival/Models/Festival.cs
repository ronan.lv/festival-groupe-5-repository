﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIFestival.Models
{
    public class Festival
    {
        public int Id { get; set; }
        [Required]
        public string Nom { get; set; }
        public string UrlLogo { get; set; }
        public string Description { get; set; }
        public string Lieu { get; set; }
        public float TarifJournee { get; set; }
        public string Hebergement { get; set; }
        public DateTime DateDeDebut { get; set; }
        public DateTime DateDeFin { get; set; }
        public int NombreDePlaces { get; set; }
        //permet de voir l'état du festival
        public Boolean RempliOuNon { get; set; }
        public Boolean PublieOuNon { get; set; }
        public Boolean InscriptionsOuvertesOuNOn { get; set; }

        public ICollection<Billet> Billets { get; set; }
        public ICollection<JouerSur> jouerSurs { get; set; }
        
        public Festival()
        {
            this.Billets = new List<Billet>();
            this.jouerSurs = new List<JouerSur>();
        }
        

    }
}
