﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIFestival.Models
{
    public class Artiste
    {
        public int Id { get; set; }
        [Required]
        public string Nom { get; set; }
        public string UrlPhoto { get; set; }
        public string PaysOrigine { get; set; }
        public string Style { get; set; }
        public string Descriptif { get; set; }
        public string UrlExtraitMusical { get; set; }
    }
}
