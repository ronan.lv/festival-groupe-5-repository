﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIFestival.Models
{
    public class Selection
    {
        public int Id { get; set; }

        [ForeignKey("FK_Festivalier")]
        public int IdFestivalier { get; set; }

        [ForeignKey("FK_Artiste")]
        public int IdArtiste { get; set; }

    }
}
