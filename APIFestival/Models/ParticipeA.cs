﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIFestival.Models
{
    public class ParticipeA
    {
        public int Id { get; set; }
        [Required]

        [ForeignKey("FK_Festivalier")]
        public int IdFestivalier { get; set; }
        [Required]

        [ForeignKey("FK_Festival")]
        public int IdFestival { get; set; }
    }
}
