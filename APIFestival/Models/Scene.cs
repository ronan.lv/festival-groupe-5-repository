﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIFestival.Models
{
    public class Scene
    {
        public int Id { get; set; }
        [Required]
        public string NomScene { get; set; }
        public string DescriptionScene { get; set; }
        public int NombreDePlaces { get; set; }
        public string Accessibilite { get; set; }

        [ForeignKey("FK_Festival")]
        public int IdFestival { get; set; } 
    }
}
