﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIFestival.Models
{
    public class Consultation
    {
        public int Id { get; set; }
        public int IdFestival { get; set; }
        public DateTime Date_enregistrement { get; set; }
    }
}
