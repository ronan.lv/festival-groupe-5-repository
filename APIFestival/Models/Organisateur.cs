﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIFestival.Models
{
    public class Organisateur
    {
        public int Id { get; set; }
        [Required]
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string AdresseMail { get; set; }
        public string NumeroTelephone { get; set; }
        public string MotDePasse { get; set; }

        [ForeignKey("FK_Festival")]
        public int IdFestival { get; set; }

    }
}
