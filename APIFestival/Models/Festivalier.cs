﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIFestival.Models
{
    public class Festivalier
    {
        public int Id { get; set; }
        [Required]
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public DateTime DateDeNaissance { get; set; }
        public string Sexe { get; set; }
        public string Pays { get; set; }
        public int CodePostal { get; set; }
        public string Commune { get; set; }
        public string Telephone { get; set; }
        public string AdresseMail { get; set; }
        public string MotDePasse { get; set; }
        public ICollection<Billet> Billets { get; set; }
        public Festivalier()
        {
            this.Billets = new List<Billet>();
        }

    }
}
