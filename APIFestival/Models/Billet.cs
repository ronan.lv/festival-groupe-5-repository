﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIFestival.Models
{
    public class Billet
    {
        public int Id { get; set; }
        public DateTime DateAchat { get; set; }

        [ForeignKey("FK_Festivalier")]
        public int IdFestivalier { get; set; }

        [ForeignKey("FK_Festival")]
        public int IdFestival { get; set; }

        public DateTime DebutFormule { get; set; }
        public DateTime FinFormule { get; set; }
        public float Montant { get; set; }
        public int Validation { get; set; }

    }
}
