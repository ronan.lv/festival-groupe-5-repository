﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIFestival.Models
{
    public class JouerSur
    {
        public int Id { get; set; }
        [Required]

        [ForeignKey("FK_Artiste")]
        public int IdArtiste { get; set; }

        public DateTime DateHeureDebutPerformance { get; set; }
        public DateTime DateHeureFinPerformance { get; set; }
        [Required]

        [ForeignKey("FK_Scène")]
        public int IdScn { get; set; }
    }
}
