﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace ClientLourdWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btlogin_Click(object sender, RoutedEventArgs e)
        {
            if (Tbmail.Text.Length > 0)
            {
                var gestionnaire = ClientLourdWPF.ControllersAPI.API.Instance.GetGestionnaire(Tbmail.Text, Pbmotdepasse.Password).Result;
                if (gestionnaire  == null)
                {
                    MessageBox.Show("Login / mot de passe incorrects !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("Bienvenue " + gestionnaire.Nom , "Connecté !", MessageBoxButton.OK, MessageBoxImage.Information);
                    Application.Current.Properties.Add("nom", gestionnaire.Nom);
                    Application.Current.Properties.Add("id", gestionnaire.Id);
                    Window2 window2 = new Window2();   //Login为窗口名，把要跳转的新窗口实例化
                    window2.Show();  //打开新窗口
                    this.Close();  //关闭当前窗口

        }

        
           
        
    }
        }

        private void BtCreer_Click(object sender, RoutedEventArgs e)
        {
            Window1 window1 = new Window1();   //Login为窗口名，把要跳转的新窗口实例化
            window1.Show();  //打开新窗口
            this.Close();  //关闭当前窗口

        }


    }
}

