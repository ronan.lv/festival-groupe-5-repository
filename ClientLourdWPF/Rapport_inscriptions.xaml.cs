﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace ClientLourdWPF
{
    /// <summary>
    /// Logique d'interaction pour Rapport_inscriptions.xaml
    /// </summary>
    public partial class Rapport_inscriptions : Window
    {
        public Rapport_inscriptions()
        {
            InitializeComponent();
            ImageBrush b = new ImageBrush();
            b.ImageSource = new BitmapImage(new Uri("ressources/window22.jpg", UriKind.Relative));
            b.Stretch = Stretch.Fill;
            this.Background = b;
            showColumnChart();
            var ListeFestivals = ClientLourdWPF.ControllersAPI.API.Instance.GetFestivalsAsync().Result;

            foreach (var f in ListeFestivals)
            {
                Combo_Festival.Items.Add(f.Nom);
            }
            for (int i = 2000; i < System.DateTime.Now.Year + 1; i++)
            {
                Combo_Année.Items.Add(i);
            }
            List<string> Months = new List<string>() { "Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre" };
            for (int i = 0; i < 12; i++)
            {
                Combo_Mois.Items.Add(Months[i]);
            }
        }

        private void showColumnChart(List<int> liste=null,List<int> ListeWeeks=null, List<int>ListeDays=null)
        {
            List<KeyValuePair<string, int>> valueList = new List<KeyValuePair<string, int>>();
            List<KeyValuePair<string, int>> valueListWeeks = new List<KeyValuePair<string, int>>();
            List<KeyValuePair<string, int>> valueListDays = new List<KeyValuePair<string, int>>();
            if (liste == null) 
            {
                valueList.Add(new KeyValuePair<string, int>("J", 0));
                valueList.Add(new KeyValuePair<string, int>("F", 0));
                valueList.Add(new KeyValuePair<string, int>("M", 0));
                valueList.Add(new KeyValuePair<string, int>("A", 0));
                valueList.Add(new KeyValuePair<string, int>("Ma", 0));
                valueList.Add(new KeyValuePair<string, int>("Juin", 0));
                valueList.Add(new KeyValuePair<string, int>("Juil", 0));
                valueList.Add(new KeyValuePair<string, int>("Ao", 0));
                valueList.Add(new KeyValuePair<string, int>("S", 0));
                valueList.Add(new KeyValuePair<string, int>("O", 0));
                valueList.Add(new KeyValuePair<string, int>("N", 0));
                valueList.Add(new KeyValuePair<string, int>("D", 0));

                for (int i = 0; i < 53; i++)
                {
                    valueListWeeks.Add(new KeyValuePair<string, int>((i + 1).ToString(), 0));
                }
                for (int i = 0; i < 32; i++)
                {
                    valueListDays.Add(new KeyValuePair<string, int>((i + 1).ToString(), 0));
                }
            }
            else
            {
                valueList.Add(new KeyValuePair<string, int>("J", liste[0]));
                valueList.Add(new KeyValuePair<string, int>("F", liste[1]));
                valueList.Add(new KeyValuePair<string, int>("M", liste[2]));
                valueList.Add(new KeyValuePair<string, int>("A", liste[3]));
                valueList.Add(new KeyValuePair<string, int>("Ma", liste[4]));
                valueList.Add(new KeyValuePair<string, int>("Juin", liste[5]));
                valueList.Add(new KeyValuePair<string, int>("Juil", liste[6]));
                valueList.Add(new KeyValuePair<string, int>("Ao", liste[7]));
                valueList.Add(new KeyValuePair<string, int>("S", liste[8]));
                valueList.Add(new KeyValuePair<string, int>("O", liste[9]));
                valueList.Add(new KeyValuePair<string, int>("N", liste[10]));
                valueList.Add(new KeyValuePair<string, int>("D", liste[11]));

                for (int i = 0; i < ListeWeeks.Count; i++)
                {
                    valueListWeeks.Add(new KeyValuePair<string, int>((i + 1).ToString(), ListeWeeks[i]));
                }
                for (int i = 0; i < ListeDays.Count; i++)
                {
                    valueListDays.Add(new KeyValuePair<string, int>((i + 1).ToString()+"th", ListeDays[i]));
                }
            }
            

            

            //Setting data for column chart
            // columnChart.DataContext = valueList;


            // Setting data for pie chart
            WeeksChart.DataContext = valueListWeeks;

            //Setting data for bar chart
            //barChart.DataContext = valueList;

            //Setting data for line chart
            DaysofMonthsChart.DataContext = valueListDays;

            //Setting data for area chart
            MonthsChart.DataContext = valueList;
        }

        private void Valider_Click(object sender, RoutedEventArgs e)
        {
            if (Combo_Festival.SelectedItem != null && Combo_Année.SelectedItem!=null && Combo_Mois.SelectedItem != null)
            {
                var liste = ClientLourdWPF.ControllersAPI.API.Instance.GetNombreInscriptions(Combo_Festival.SelectedItem.ToString(), Combo_Année.SelectedItem.ToString()).Result;
                var ListeWeeks = ClientLourdWPF.ControllersAPI.API.Instance.GetNombreInscritSemaine(Combo_Festival.SelectedItem.ToString(), Combo_Année.SelectedItem.ToString()).Result;
                int mois = 0;
                if (Combo_Mois.SelectedItem.ToString() == "Janvier") { mois = 1; }
                if (Combo_Mois.SelectedItem.ToString() == "Fevrier") { mois = 2; }
                if (Combo_Mois.SelectedItem.ToString() == "Mars") { mois = 3; }
                if (Combo_Mois.SelectedItem.ToString() == "Avril") { mois = 4; }
                if (Combo_Mois.SelectedItem.ToString() == "Mai") { mois = 5; }
                if (Combo_Mois.SelectedItem.ToString() == "Juin") { mois = 6; }
                if (Combo_Mois.SelectedItem.ToString() == "Juillet") { mois = 7; }
                if (Combo_Mois.SelectedItem.ToString() == "Août") { mois = 8; }
                if (Combo_Mois.SelectedItem.ToString() == "Septembre") { mois = 9; }
                if (Combo_Mois.SelectedItem.ToString() == "Octobre") { mois = 10; }
                if (Combo_Mois.SelectedItem.ToString() == "Novembre") { mois = 11; }
                if (Combo_Mois.SelectedItem.ToString() == "Decembre") { mois = 12; }

                var ListeDays = ClientLourdWPF.ControllersAPI.API.Instance.GetNombreInscritJour(Combo_Festival.SelectedItem.ToString(), Combo_Année.SelectedItem.ToString(), mois).Result;
                showColumnChart(liste, ListeWeeks, ListeDays);
            }
            else
            {
                MessageBox.Show("Remplissez tous les champs", "Erreur",MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void RETOUR_Click(object sender, RoutedEventArgs e)
        {
            Window2 window2 = new Window2();
            Application.Current.Properties.Clear();
            window2.Show();
            this.Close();
        }
    }
}
