﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using APIFestival.Models;

namespace ClientLourdWPF
{
    /// <summary>
    /// Window2.xaml 的交互逻辑
    /// </summary>
    public partial class Window2 : Window
    {
        public Window2()
        {
            InitializeComponent();

            ImageBrush b = new ImageBrush();
            b.ImageSource = new BitmapImage(new Uri("ressources/window2.jpg", UriKind.Relative));
            b.Stretch = Stretch.Fill;
            this.Background = b;

        }

        

        private void Btretour_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainwindow = new MainWindow();   //Login为窗口名，把要跳转的新窗口实例化
            Application.Current.Properties.Clear();
            mainwindow.Show();  //打开新窗口
            this.Close();  //关闭当前窗口
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Window3 window3 = new Window3();   //Login为窗口名，把要跳转的新窗口实例化
            Application.Current.Properties.Clear();
            window3.Show();  //打开新窗口
            this.Close();  //关闭当前窗口
        }

        private void Btmodif_Click_1(object sender, RoutedEventArgs e)
        {
            Window4 window4 = new Window4();   //Login为窗口名，把要跳转的新窗口实例化
            Application.Current.Properties.Clear();
            window4.Show();  //打开新窗口
            this.Close();  //关闭当前窗口
        }

        private void Btinfo_Click_1(object sender, RoutedEventArgs e)
        {
           Rapport_Festivalier rapport = new Rapport_Festivalier();   //Login为窗口名，把要跳转的新窗口实例化
            Application.Current.Properties.Clear();
            rapport.Show();  //打开新窗口
            this.Close();  //关闭当前窗口
        }

        private void Btinscrip_Click(object sender, RoutedEventArgs e)
        {
            Rapport_inscriptions r = new Rapport_inscriptions();
            Application.Current.Properties.Clear();
            r.Show();
            this.Close();   //Login为窗口名，把要跳转的新窗口实例化
        }

        private void Btvente_Click(object sender, RoutedEventArgs e)
        {

            Rapport_inscriptions r = new Rapport_inscriptions();
            Application.Current.Properties.Clear();
            r.Show();
            this.Close();   //Login为窗口名，把要跳转的新窗口实例化
        }

        private void Btv_Click(object sender, RoutedEventArgs e)
        {

            Rapport_vente r = new Rapport_vente();
            Application.Current.Properties.Clear();
            r.Show();
            this.Close();   //Login为窗口名，把要跳转的新窗口实例化
        }
       
          
        }
    }

