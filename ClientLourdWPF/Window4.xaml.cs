﻿using APIFestival.Models;
using ClientLourdWPF.ControllersAPI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientLourdWPF
{
    /// <summary>
    /// Window4.xaml 的交互逻辑
    /// </summary>
    public partial class Window4 : Window

    {
        private List<Organisateur> listeOrganisateursDuFestivalActuel;
        private List<Organisateur> listeDeToutLesOrganisateurs;
        private Festival festivalSelectionne;

        public Window4()
        {
            InitializeComponent();
            listeBoxFestivals.ItemsSource = ClientLourdWPF.ControllersAPI.API.Instance.GetFestivalsAsync().Result; ;
            listeBoxFestivals.DisplayMemberPath = "Nom";

            listeDeToutLesOrganisateurs = API.Instance.GetOrganisateursAsync().Result.ToList<Organisateur>();
            comboBoxOrganisateurs.ItemsSource = listeDeToutLesOrganisateurs;
            comboBoxOrganisateurs.Items.SortDescriptions.Add(new SortDescription("Nom", ListSortDirection.Descending));
            comboBoxOrganisateurs.DisplayMemberPath = "Nom";

            panelWithControls.IsEnabled = false;
        }


        private void Button_Click_Charger(object sender, RoutedEventArgs e)
        {
            if (listeBoxFestivals.SelectedItems.Count == 1)
            {
                panelWithControls.IsEnabled = true;
                comboBoxOrganisateurs.SelectedItem = comboBoxOrganisateurs.Items.GetItemAt(0);

                //获取选中的值
                festivalSelectionne = (Festival) listeBoxFestivals.SelectedItem;
                if(Application.Current.Properties.Contains("nom"))
                    Application.Current.Properties.Remove("nom");
                Application.Current.Properties.Add("nom", listeBoxFestivals.SelectedItem.ToString());
                Tbnom.Text = festivalSelectionne.Nom;
                Tblieu.Text = festivalSelectionne.Lieu;
                Tbdes.Text = festivalSelectionne.Description;

                //Requête LINQ (similaire au requêtes SQL, mais applicable dans des collections
                listeOrganisateursDuFestivalActuel = (from orga in listeDeToutLesOrganisateurs where orga.IdFestival == festivalSelectionne.Id select orga).ToList<Organisateur>();
                panelOrganisateursDuFestivalActuel.Children.Clear();
                foreach (Organisateur organisateur in listeOrganisateursDuFestivalActuel)
                {
                    ajouterUnOrganisateurDansAffichage(organisateur);
                }

                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad; //Permet de de fermer le fichier une fois l'image chargée
                bitmapImage.UriSource = new Uri("../../../../SiteWebFestival/wwwroot" + festivalSelectionne.UrlLogo, UriKind.Relative);
                bitmapImage.EndInit();
                image2.Source = bitmapImage;

                if (Application.Current.Properties.Contains("IdF"))
                    Application.Current.Properties.Remove("IdF");
                Application.Current.Properties.Add("IdF", festivalSelectionne.Id);



            }
            else
            {
                MessageBox.Show("Sélectionnez un festival");
            }
        }

        private void Button_Click_Ajouter(object sender, RoutedEventArgs e)
        {
           if (!listeOrganisateursDuFestivalActuel.Contains((Organisateur)comboBoxOrganisateurs.SelectedItem))
            {
                listeOrganisateursDuFestivalActuel.Add((Organisateur)comboBoxOrganisateurs.SelectedItem);
                ajouterUnOrganisateurDansAffichage((Organisateur)comboBoxOrganisateurs.SelectedItem);
            }
        }
        private void ajouterUnOrganisateurDansAffichage(Organisateur organisateur)
        {
            DockPanel panel = new DockPanel();

            Label labelOrganisateurAjoute = new Label();
            labelOrganisateurAjoute.Content = organisateur.Nom;
            labelOrganisateurAjoute.Margin = new Thickness(80, 0, 0, 0);

            Image imageDelete = new Image();
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad; //Permet de de fermer le fichier une fois l'image chargée
            bitmapImage.UriSource = new Uri("ressources/delete.png", UriKind.Relative);
            bitmapImage.EndInit();
            imageDelete.Source = bitmapImage;
            imageDelete.Width = 19;
            imageDelete.Height = 19;
            imageDelete.HorizontalAlignment = HorizontalAlignment.Right;
            imageDelete.MouseLeftButtonDown += new MouseButtonEventHandler(supprimerUnOrganisateur_MouseLeftButtonDown);
            imageDelete.DataContext = organisateur;

            panel.Children.Add(labelOrganisateurAjoute);
            panel.Children.Add(imageDelete);

            panelOrganisateursDuFestivalActuel.Children.Add(panel);
        }

        private void supprimerUnOrganisateur_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            Organisateur organisateurASUpprimer = (Organisateur)((Image)sender).DataContext;

            DockPanel panelASupprimer = new DockPanel();
            foreach (DockPanel panel in panelOrganisateursDuFestivalActuel.Children)
            {
                if (((Organisateur)((Image)panel.Children[1]).DataContext).Equals(organisateurASUpprimer))
                    panelASupprimer = panel;
            }
            listeOrganisateursDuFestivalActuel.Remove(organisateurASUpprimer);
            panelOrganisateursDuFestivalActuel.Children.Remove(panelASupprimer);
        }

        private void Button_Click_Valider(object sender, RoutedEventArgs e)
        {
            string filepath = @image2.Source.ToString();
            string path = filepath.Substring(filepath.LastIndexOf("///") + 3);
            string filename = filepath.Substring(filepath.LastIndexOf('/') + 1);
            string uri = "./../../../../../festival-groupe-5-repository/SiteWebFestival/wwwroot/images/festivals/" + filename;
            
            festivalSelectionne.Nom = Tbnom.Text;
            festivalSelectionne.Lieu = Tblieu.Text;
            festivalSelectionne.Description = Tbdes.Text;
            festivalSelectionne.UrlLogo = "/images/festivals/" + filename;
            var uri1 = ClientLourdWPF.ControllersAPI.API.Instance.ModifFesitval(festivalSelectionne);

            List<Organisateur> listeOrgaAvecIdDuFestival = (from orga in listeDeToutLesOrganisateurs where orga.IdFestival == festivalSelectionne.Id select orga).ToList<Organisateur>();


            //Permet de modifier tous les organisateurs affichés
            foreach (Organisateur orga in listeOrganisateursDuFestivalActuel)
            {          
                if (orga.IdFestival != festivalSelectionne.Id)
                {
                    orga.IdFestival = festivalSelectionne.Id;
                    _ = API.Instance.ModifOrganisateur(orga);
                }
            }

            //Permet d'enlever le festivals aux organisateurs qu'on a supprimé
            foreach(Organisateur orga in listeOrgaAvecIdDuFestival)
            {
                if (!listeOrganisateursDuFestivalActuel.Contains(orga))
                {
                    orga.IdFestival = -1;
                    _ = API.Instance.ModifOrganisateur(orga);
                }
            }
            
            /*
            System.Net.WebClient myWebClient = new System.Net.WebClient();
            myWebClient.UploadFile(uri, "put", path);
            myWebClient.Dispose();
            */


            MessageBox.Show("Modification reussit !", "Enregistrement effectué", MessageBoxButton.OK, MessageBoxImage.Information);

            Window2 window2 = new Window2();   //Login为窗口名，把要跳转的新窗口实例化
            Application.Current.Properties.Clear();
            window2.Show();  //打开新窗口
            this.Close();  //关闭当前窗口
        }

        private void Button_Click_Parcourir(object sender, RoutedEventArgs e)
        {
            {
                OpenFileDialog openfiledialog = new OpenFileDialog
                {
                    Filter = "图像文件|*.jpg;*.png;*.jpeg;*.bmp;*.gif|所有文件|*.*"
                };

                if ((bool)openfiledialog.ShowDialog())
                {
                    image2.Source = new BitmapImage(new Uri(openfiledialog.FileName));

                }
            }
        }





        private void Button_Click_Retour(object sender, RoutedEventArgs e)
        {
            Window2 window2 = new Window2();   //Login为窗口名，把要跳转的新窗口实例化
            Application.Current.Properties.Clear();
            window2.Show();  //打开新窗口
            this.Close();  //关闭当前窗口
        }

    }
}

