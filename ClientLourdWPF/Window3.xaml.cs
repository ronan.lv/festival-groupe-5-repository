﻿using APIFestival.Models;
using ClientLourdWPF.ControllersAPI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientLourdWPF
{
    /// <summary>
    /// Window3.xaml 的交互逻辑
    /// </summary>
    public partial class Window3 : Window

    {
        private readonly ICollection<Organisateur> ListeOrganisateurs;
        private List<Organisateur> listeOrganisateursAjoutes;


        public Window3()
        {

            InitializeComponent();
            ListeOrganisateurs = ClientLourdWPF.ControllersAPI.API.Instance.GetOrganisateursAsync().Result;
            selectionOrganisateur.ItemsSource = ListeOrganisateurs;
            selectionOrganisateur.Items.SortDescriptions.Add(new SortDescription("Nom", ListSortDirection.Descending));
            selectionOrganisateur.DisplayMemberPath = "Nom";

            listeOrganisateursAjoutes = new List<Organisateur>();

        }

        private void Button_Click_Retour(object sender, RoutedEventArgs e)
        {
            Window2 window2 = new Window2();   //Login为窗口名，把要跳转的新窗口实例化
            Application.Current.Properties.Clear();
            window2.Show();  //打开新窗口
            this.Close();  //关闭当前窗口
        }



        private void Button_Click_Ajouter(object sender, RoutedEventArgs e)
        {
            if (!listeOrganisateursAjoutes.Contains((Organisateur)selectionOrganisateur.SelectedItem)){

                listeOrganisateursAjoutes.Add((Organisateur)selectionOrganisateur.SelectedItem);

                DockPanel panel = new DockPanel();

                Label labelOrganisateurAjoute = new Label();
                labelOrganisateurAjoute.Content = ((Organisateur)selectionOrganisateur.SelectedItem).Nom;
                labelOrganisateurAjoute.Margin = new Thickness(100, 0, 0, 0);

                Image imageDelete = new Image();
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad; //Permet de de fermer le fichier une fois l'image chargée
                bitmapImage.UriSource = new Uri("ressources/delete.png", UriKind.Relative);
                bitmapImage.EndInit();
                imageDelete.Source = bitmapImage;
                imageDelete.Width = 19;
                imageDelete.Height = 19;
                imageDelete.HorizontalAlignment = HorizontalAlignment.Right;
                imageDelete.MouseLeftButtonDown += new MouseButtonEventHandler(supprimerUnOrganisateur_MouseLeftButtonDown);
                imageDelete.DataContext = (Organisateur) selectionOrganisateur.SelectedItem;

                panel.Children.Add(labelOrganisateurAjoute);
                panel.Children.Add(imageDelete);

                panelOrganisateursAjoutes.Children.Add(panel);
            }
        }

        private void supprimerUnOrganisateur_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            Organisateur organisateurASUpprimer = (Organisateur) ((Image) sender).DataContext;

            DockPanel panelASupprimer = new DockPanel();
            foreach (DockPanel panel in panelOrganisateursAjoutes.Children)
            {
                if (((Organisateur)((Image)panel.Children[1]).DataContext).Equals(organisateurASUpprimer))
                    panelASupprimer = panel;
            }
            listeOrganisateursAjoutes.Remove(organisateurASUpprimer);
            panelOrganisateursAjoutes.Children.Remove(panelASupprimer);
        }

        private void Button_Click_Creer(object sender, RoutedEventArgs e)
        {
            string filepath = @Image1.Source.ToString();
            string path = filepath.Substring(filepath.LastIndexOf("///") + 3);
            string filename = filepath.Substring(filepath.LastIndexOf('/') + 1);
            string uri = "./../../../../../festival-groupe-5-repository/SiteWebFestival/wwwroot/images/festivals/" + filename;


            if (Tbnom.Text.Length > 0)
            {
                Festival festival1 = ClientLourdWPF.ControllersAPI.API.Instance.GetFestivalByNom(Tbnom.Text).Result;
                if (festival1 == null)
                {


                    List<Festival> Liste = new List<Festival>();
                    Liste = (List<Festival>)API.Instance.GetFestivalsAsync().Result;
                    int max = 0;
                    for (int k = 0; k < Liste.Count; k++)
                    {
                        if (max < Liste[k].Id)
                            max = Liste[k].Id;
                    }

                   
                    foreach(Organisateur organisateur in listeOrganisateursAjoutes)
                    {
                        organisateur.IdFestival = max + 1;
                        var uri2 = ClientLourdWPF.ControllersAPI.API.Instance.ModifOrganisateur(organisateur);
                    }


                    Festival festival = new Festival
                    {
                        Lieu = Tblp.Text,
                        Nom = Tbnom.Text,
                        Description = Tbdis.Text,
                        UrlLogo = "/images/festivals/" + filename,

                    }; System.Net.WebClient myWebClient = new System.Net.WebClient();
                    myWebClient.UploadFile(uri, "put", path);
                    myWebClient.Dispose();
                    _ = ClientLourdWPF.ControllersAPI.API.Instance.CreerFestival(festival);


                    MessageBox.Show("Festival ajoutée !", "Enregistrement effectué", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Le nom de le festival existe déjà !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            else
            {
                MessageBox.Show("Les infos doivent être renseigné !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }



        private void Button_Click_Parcourir(object sender, RoutedEventArgs e)
        {
            {
                OpenFileDialog openfiledialog = new OpenFileDialog
                {
                    Filter = "图像文件|*.jpg;*.png;*.jpeg;*.bmp;*.gif|所有文件|*.*"
                };

                if ((bool)openfiledialog.ShowDialog())
                {
                    Image1.Source = new BitmapImage(new Uri(openfiledialog.FileName));

                }
            }
        }

        private void selectionOrganisateur_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
    



    



        
      
    