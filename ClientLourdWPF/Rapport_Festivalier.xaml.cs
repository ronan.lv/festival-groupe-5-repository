﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using APIFestival.Models;
using Newtonsoft.Json;



namespace ClientLourdWPF
{
    /// <summary>
    /// Logique d'interaction pour Rapport_Festivalier.xaml
    /// </summary>
    public partial class Rapport_Festivalier : Window
    {
        public Rapport_Festivalier()
        {
            InitializeComponent();
            ImageBrush b = new ImageBrush();
            b.ImageSource = new BitmapImage(new Uri("ressources/window22.jpg", UriKind.Relative));
            b.Stretch = Stretch.Fill;
            this.Background = b;

            var ListeFestivals = ClientLourdWPF.ControllersAPI.API.Instance.GetFestivalsAsync().Result;

            foreach(var f in ListeFestivals)
            {
                Combo_Festival.Items.Add(f.Nom);
            }
            List<String> Countries = new List<string> { "France", "Angleterre", "USA", "Belgique", "Danemark", "Allemagne", "Ethiopie", "Maroc", "Algérie", "Cameroun", "Chine", "Japon", "Inde" };
            
            foreach(var p in Countries)
            {
                this.Combo_Pays.Items.Add(p);
            }

            this.Combo_Pays.SelectedIndex = 0;

            if (Combo_Pays.SelectedIndex == 0)
            {
                Combo_Region.Visibility = Visibility.Visible;
                Combo_Departement.Visibility = Visibility.Visible;
                StreamReader sr = new StreamReader("../Ressources/numDepartementsEtRegions.csv"); //Permet d'ouvrir le fichier
                String line;
                String[] ligneSeparee; //C'est ici que l'on va stocker la ligne lue (qui est donc séparéée en trois strings distinctes)

                while ((line = sr.ReadLine()) != null)  //on lit le fichier ligne par ligne, et on continue tant qu'on est pas arrivé au bout du fichier
                {
                    ligneSeparee = line.Split(","); //permet de séparer la ligne en plusieurs sous chaines, en focntion de la virgule
                    if (!Combo_Departement.Items.Contains(ligneSeparee[1]))
                    {
                        Combo_Departement.Items.Add(ligneSeparee[1]); //la deuxième string séparée correspond au nom de département
                    }
                    if (!Combo_Region.Items.Contains(ligneSeparee[2]))
                    {
                        Combo_Region.Items.Add(ligneSeparee[2]); //la troisième string séparée correspond au nom de région
                    }
                    
                }
            }
            else
            {
                Combo_Region.Visibility = Visibility.Hidden;

                Combo_Departement.Visibility = Visibility.Hidden;
            }
            

        }



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            List<int> liste = new List<int>();
            if (Combo_Festival.SelectedItem!=null && Combo_Pays.SelectedItem!=null)
            {
                if (Combo_Region.Visibility == Visibility.Hidden)
                {
                    
                    liste = ClientLourdWPF.ControllersAPI.API.Instance.GetNombreDeFestivaliersTotal(Combo_Festival.SelectedItem.ToString(), Combo_Pays.SelectedItem.ToString()).Result;

                    Texte_NombrePays.Text = liste[0].ToString();
                    Texte_NombreRegion.Text = "No View";
                    if (liste[0] != 0)
                    {
                        Barre_Departement.Value = 0;
                        Barre_Hommes.Value = (liste[3] * 100.00) / liste[0];
                        Barre_Femmes.Value = (liste[4] * 100.00) / liste[0];
                        Barre_Neutre.Value = (liste[8] * 100.00) / liste[0];
                        Barre_NR.Value = (liste[9] * 100.00) / liste[0];
                    }
                    else
                    {
                        Barre_Departement.Value = 0;
                        Barre_Hommes.Value = 0;
                        Barre_Femmes.Value = 0;
                        Barre_Neutre.Value = 0;
                        Barre_NR.Value = 0;
                    }


                    Text_Femmes.Text = Barre_Femmes.Value.ToString("N2") + " %";
                    Text_Hommes.Text = Barre_Hommes.Value.ToString("N2") + " %";
                    Text_Neutre.Text= Barre_Neutre.Value.ToString("N2") + " %";
                    Text_NR.Text = Barre_NR.Value.ToString("N2") + " %";
                    Text_Departement.Text = "No View";
                    if (liste[0] != 0)
                    {
                        Barre_Enfants.Value = (liste[7] * 100.00) / liste[0];
                        Barre_Adolescents.Value = (liste[6] * 100.00) / liste[0];
                        Barre_Adultes.Value = (liste[5] * 100.00) / liste[0];
                    }
                    else
                    {
                        Barre_Enfants.Value = 0;
                        Barre_Adolescents.Value = 0;
                        Barre_Adultes.Value = 0;
                    }


                    Text_Enfant.Text = Barre_Enfants.Value.ToString("N2") + " %";
                    Text_Adolescent.Text = Barre_Adolescents.Value.ToString("N2") + " %";
                    Text_Adultes.Text = Barre_Adultes.Value.ToString("N2") + " %";

                }
                else
                {
                    if(Combo_Region.SelectedItem!=null && Combo_Departement.SelectedItem != null)
                    {
                        liste = ClientLourdWPF.ControllersAPI.API.Instance.GetNombreDeFestivaliersTotal(Combo_Festival.SelectedItem.ToString(), Combo_Pays.SelectedItem.ToString(), Combo_Departement.SelectedItem.ToString(), Combo_Region.SelectedItem.ToString()).Result;

                        Texte_NombrePays.Text = liste[0].ToString();
                        Texte_NombreRegion.Text = liste[1].ToString();
                        if (liste[0] != 0)
                        {
                            Barre_Departement.Value = (liste[2] * 100.00) / liste[0];
                            Barre_Hommes.Value = (liste[3] * 100.00) / liste[2];
                            Barre_Femmes.Value = (liste[4] * 100.00) / liste[2];
                            Barre_Neutre.Value = (liste[8] * 100.00) / liste[2];
                            Barre_NR.Value = (liste[9] * 100.00) / liste[2];
                        }
                        else
                        {
                            Barre_Departement.Value = 0;
                            Barre_Hommes.Value = 0;
                            Barre_Femmes.Value = 0;
                            Barre_Neutre.Value = 0;
                            Barre_NR.Value = 0;
                        }


                        Text_Femmes.Text = Barre_Femmes.Value.ToString("N2") + " %";
                        Text_Hommes.Text = Barre_Hommes.Value.ToString("N2") + " %";
                        Text_Neutre.Text = Barre_Neutre.Value.ToString("N2") + " %";
                        Text_NR.Text = Barre_NR.Value.ToString("N2") + " %";
                        Text_Departement.Text = Barre_Departement.Value.ToString("N2") + " %";
                        if (liste[2] != 0)
                        {
                            Barre_Enfants.Value = (liste[7] * 100.00) / liste[2];
                            Barre_Adolescents.Value = (liste[6] * 100.00) / liste[2];
                            Barre_Adultes.Value = (liste[5] * 100.00) / liste[2];
                        }
                        else
                        {
                            Barre_Enfants.Value = 0;
                            Barre_Adolescents.Value = 0;
                            Barre_Adultes.Value = 0;
                        }


                        Text_Enfant.Text = Barre_Enfants.Value.ToString("N2") + " %";
                        Text_Adolescent.Text = Barre_Adolescents.Value.ToString("N2") + " %";
                        Text_Adultes.Text = Barre_Adultes.Value.ToString("N2") + " %";
                    }
                    else
                    {
                        MessageBox.Show("Choisissez tous les filtres", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                }

            }
            else
            {
                MessageBox.Show("Choisissez tous les filtres", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Combo_Pays.SelectedIndex == 0)
            {
                Combo_Region.Visibility = Visibility.Visible;
                Combo_Departement.Visibility = Visibility.Visible;
                StreamReader sr = new StreamReader("../Ressources/numDepartementsEtRegions.csv"); //Permet d'ouvrir le fichier
                String line;
                String[] ligneSeparee; //C'est ici que l'on va stocker la ligne lue (qui est donc séparéée en trois strings distinctes)

                while ((line = sr.ReadLine()) != null)  //on lit le fichier ligne par ligne, et on continue tant qu'on est pas arrivé au bout du fichier
                {
                    ligneSeparee = line.Split(","); //permet de séparer la ligne en plusieurs sous chaines, en focntion de la virgule
                    if (!Combo_Departement.Items.Contains(ligneSeparee[1]))
                    {
                        Combo_Departement.Items.Add(ligneSeparee[1]); //la deuxième string séparée correspond au nom de département
                    }
                    if (!Combo_Region.Items.Contains(ligneSeparee[2]))
                    {
                        Combo_Region.Items.Add(ligneSeparee[2]); //la troisième string séparée correspond au nom de région
                    }
                }
            }
            else
            {
                Combo_Region.Visibility=Visibility.Hidden;
                Combo_Departement.Visibility=Visibility.Hidden;
                
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Window2 window2 = new Window2();
            Application.Current.Properties.Clear();
            window2.Show(); 
            this.Close(); 
        }
    }
}
