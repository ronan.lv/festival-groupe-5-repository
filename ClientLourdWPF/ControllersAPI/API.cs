﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using APIFestival.Models;
using Newtonsoft.Json;

namespace ClientLourdWPF.ControllersAPI
{
    public sealed class API
    {
        private static readonly HttpClient client = new HttpClient();

        private API()
        {
            client.BaseAddress = new Uri("http://localhost:54566");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
        private static readonly object padlock = new object();
        private static API instance = null;

        public static API Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new API();
                    }
                    return instance;
                }
            }
        }
        public async Task<Gestionnaire > GetGestionnaireByAdresseMail(string AdresseMail)
        {
            Gestionnaire gestionnaire  = null;
            HttpResponseMessage response = client.GetAsync("api/Gestionnaires/AdresseMail/" + AdresseMail).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                gestionnaire  = JsonConvert.DeserializeObject<Gestionnaire >(resp);
            }
            return gestionnaire ;
        }
        public async Task<Gestionnaire > GetGestionnaire(string AdresseMail, string MotDePasse)
        {
            Gestionnaire  gestionnaire = null;
            HttpResponseMessage response = client.GetAsync("api/Gestionnaires/" + AdresseMail + "/" + MotDePasse).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                gestionnaire  = JsonConvert.DeserializeObject<Gestionnaire >(resp);
            }
            return gestionnaire ;
        }
        public async Task<Uri> AjoutGestionnaire(Gestionnaire  gestionnaire)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Gestionnaires", gestionnaire );
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        public async Task<Uri> CreerFestival(Festival  festival )
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Festivals", festival);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        public async Task<Festival > GetFestivalByNom(string Nom)
        {
            Festival festival  = null;
            HttpResponseMessage response = client.GetAsync("api/Festivals/Nom/" + Nom).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                festival  = JsonConvert.DeserializeObject<Festival >(resp);
            }
            return festival ;
        }
        public async Task<ICollection<Festival>> GetFestivalsAsync()
        {
            ICollection<Festival> festivals = new List<Festival>();
            HttpResponseMessage response = client.GetAsync("api/Festivals").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                festivals = JsonConvert.DeserializeObject<List<Festival>>(resp);
            }
            return festivals;
        }
        public async Task<Organisateur> GetOrganisateur(int Id)
        {
            Organisateur organisateur = null;
            HttpResponseMessage response = client.GetAsync("api/Organisateurs/Site/Web/" + Id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                organisateur = JsonConvert.DeserializeObject<Organisateur>(resp);
            }
            return organisateur;
        }
        public async Task<Organisateur> GetOrganisateurByNom(string Nom)
        {
            Organisateur organisateur = null;
            HttpResponseMessage response = client.GetAsync("api/Organisateurs/Nom/" + Nom).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                organisateur = JsonConvert.DeserializeObject<Organisateur>(resp);
            }
            return organisateur;

        }
        public async Task<Organisateur> GetOrganisateurByIdFestival(int IdFestival)
        {
            Organisateur organisateur = null;
            HttpResponseMessage response = client.GetAsync("api/Organisateurs/IdFestival/" + IdFestival).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                organisateur = JsonConvert.DeserializeObject<Organisateur>(resp);
            }
            return organisateur;

        }
        public async Task<Uri> ModifOrganisateur(Organisateur organisateur)
        {
            try
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("api/Organisateurs/" + organisateur.Id, organisateur);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        public async Task<Uri> ModifFesitval(Festival festival)
        {
            try
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("api/Festivals/" + festival.Id, festival);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        public async Task<ICollection<Organisateur>> GetOrganisateursAsync()
        {
            ICollection<Organisateur> organisateurs = new List<Organisateur>();
            HttpResponseMessage response = client.GetAsync("api/Organisateurs").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                organisateurs = JsonConvert.DeserializeObject<List<Organisateur>>(resp);
            }
            return organisateurs;
        }

        public async Task<List<int>> GetNombreDeFestivaliersTotal(string NomFestivalActuel, string Pays,string Departement=null, string Region=null)
        {
            string codePostal;
            codePostal = null;


            HttpResponseMessage rep = client.GetAsync("api/Festivals/" + NomFestivalActuel+"/w/w").Result;
            var resp = await rep.Content.ReadAsStringAsync();
            int IdFestivalActuel = JsonConvert.DeserializeObject<int>(resp);


            ICollection<Festivalier> ListeFestivaliers = null;
            HttpResponseMessage response = client.GetAsync("api/Festivals/Festivaliers/" + IdFestivalActuel).Result;
            if (response.IsSuccessStatusCode)
            {
                resp = await response.Content.ReadAsStringAsync();
                ListeFestivaliers = JsonConvert.DeserializeObject<ICollection<Festivalier>>(resp);
            }
            int NombreFestivaliers = 0;
            int NombreFestivaliersRegion = 0;
            int NombreFestivaliersPays = 0;
            int NombreFemmes = 0;
            int NombreHommes = 0;
            int NombreNeutre = 0;
            int NombreNR = 0;
            int Enfant = 0;
            int Adolescent = 0;
            int Adulte = 0;
            if (Pays == "France")
            {
                foreach (Festivalier f in ListeFestivaliers)
                {
                    NombreFestivaliersPays++;
                    codePostal = f.CodePostal.ToString();
                    string dpt = null;
                    string rgn = null;

                    StreamReader sr = new StreamReader("../Ressources/numDepartementsEtRegions.csv"); //Permet d'ouvrir le fichier
                    String line;
                    if (codePostal.Substring(0, 2).Equals("97")) //Le cas des code postaux commençant par 97 est un peu particulier, voir le fichier .csv
                    {
                        codePostal = codePostal.Substring(0, 3); //Si departement outre mer on garde les 3 premiers chiffres du code postal
                    }
                    else
                    {
                        codePostal = codePostal.Substring(0, 2); //sinon les deux premiers
                    }

                    String[] ligneSeparee; //C'est ici que l'on va stocker la ligne lue (qui est donc séparéée en trois strings distinctes)

                    while ((line = sr.ReadLine()) != null)  //on lit le fichier ligne par ligne, et on continue tant qu'on est pas arrivé au bout du fichier
                    {
                        ligneSeparee = line.Split(","); //permet de séparer la ligne en plusieurs sous chaines, en focntion de la virgule
                        if (ligneSeparee[0].Equals(codePostal)) //la première string séparée correspond au numéro de département. Ici on vérifie si le codePostal rentré correspond au code de département dans le fichier
                        {
                            dpt = ligneSeparee[1]; //la deuxième string séparée correspond au nom de département
                            rgn = ligneSeparee[2]; //la troisième string séparée correspond au nom de région
                        }
                    }
                    if (rgn == Region )
                    {
                        NombreFestivaliersRegion++;
                        if(dpt == Departement) 
                        { 

                        NombreFestivaliers++;
                        if (f.Sexe == "Femme")
                        {
                            NombreFemmes++;

                        }
                        if(f.Sexe=="Homme")
                        {
                            NombreHommes++;
                        }
                            if (f.Sexe == "Neutre")
                            {
                                NombreNeutre++;
                            }
                            if (f.Sexe == "NR")
                            {
                                NombreNR++;
                            }

                        if ((System.DateTime.Now - f.DateDeNaissance).TotalDays < 4745)
                        {
                            Enfant++;
                        }
                        else
                        {
                            if ((System.DateTime.Now - f.DateDeNaissance).TotalDays < 6570 && (System.DateTime.Now - f.DateDeNaissance).TotalDays > 4745)
                            {
                                Adolescent++;
                            }
                            else
                            {
                                Adulte++;
                            }
                        }
                    }
                    }
                }
            }
            else
            {
                foreach (var f in ListeFestivaliers)
                {
                    
                    if (f.Pays == Pays)
                    {
                        NombreFestivaliersPays++;
                        if (f.Sexe == "Femme")
                        {
                            NombreFemmes++;

                        }
                        if (f.Sexe == "Homme")
                        {
                            NombreHommes++;
                        }
                        if (f.Sexe == "Neutre")
                        {
                            NombreNeutre++;
                        }
                        if (f.Sexe == "NR")
                        {
                            NombreNR++;
                        }
                        if ((System.DateTime.Now - f.DateDeNaissance).TotalDays < 4745)
                        {
                            Enfant++;
                        }
                        else
                        {
                            if ((System.DateTime.Now - f.DateDeNaissance).TotalDays < 6570 && (System.DateTime.Now - f.DateDeNaissance).TotalDays > 4745)
                            {
                                Adolescent++;
                            }
                            else
                            {
                                Adulte++;
                            }
                        }
                    }
                }

            }

            // List<int> NombresFestivaliers = new List<int> { NombreFestivaliersPays, NombreFestivaliersRegion, NombreFestivaliers, NombreHommes, NombreFemmes, Adulte, Adolescent, Enfant };
            List<int> NombresFestivaliers = new List<int> ();
            NombresFestivaliers.Add(NombreFestivaliersPays);
            NombresFestivaliers.Add(NombreFestivaliersRegion);
            NombresFestivaliers.Add(NombreFestivaliers);
            NombresFestivaliers.Add(NombreHommes);
            NombresFestivaliers.Add(NombreFemmes);
            NombresFestivaliers.Add(Adulte);
            NombresFestivaliers.Add(Adolescent);
            NombresFestivaliers.Add(Enfant);
            NombresFestivaliers.Add(NombreNeutre);
            NombresFestivaliers.Add(NombreNR);

            return NombresFestivaliers;
        }

        public async Task<List<int>> GetNombreInscriptions(string NomFestivalActuel, String Date)
        {
            HttpResponseMessage rep = client.GetAsync("api/Festivals/" + NomFestivalActuel + "/w/w").Result;
            var resp = await rep.Content.ReadAsStringAsync();
            int IdFestivalActuel = JsonConvert.DeserializeObject<int>(resp);

            HttpResponseMessage response = client.GetAsync("api/Billets/Festival/" + IdFestivalActuel).Result;
            ICollection<Billet> ListeBillets = null;
            if (response.IsSuccessStatusCode)
            {
                resp = await response.Content.ReadAsStringAsync();
                ListeBillets = JsonConvert.DeserializeObject<ICollection<Billet>>(resp);
            }
            List<int> Inscriptions = new List<int>();
            for(int i = 0; i < 12; i++)
            {
                Inscriptions.Add(0);
            }

            foreach(Billet b in ListeBillets)
            {
                if (b.DateAchat.Year.ToString() == Date)
                {
                    if (b.DateAchat.Month == 1)
                    {
                        Inscriptions[0]++;
                    }
                    if (b.DateAchat.Month == 2)
                    {
                        Inscriptions[1]++;
                    }
                    if (b.DateAchat.Month == 3)
                    {
                        Inscriptions[2]++;
                    }
                    if (b.DateAchat.Month == 4)
                    {
                        Inscriptions[3]++;
                    }
                    if (b.DateAchat.Month == 5)
                    {
                        Inscriptions[4]++;
                    }
                    if (b.DateAchat.Month == 6)
                    {
                        Inscriptions[5]++;
                    }
                    if (b.DateAchat.Month == 7)
                    {
                        Inscriptions[6]++;
                    }
                    if (b.DateAchat.Month == 8)
                    {
                        Inscriptions[7]++;
                    }
                    if (b.DateAchat.Month == 9)
                    {
                        Inscriptions[8]++;
                    }
                    if (b.DateAchat.Month == 10)
                    {
                        Inscriptions[9]++;
                    }
                    if (b.DateAchat.Month == 11)
                    {
                        Inscriptions[10]++;
                    }
                    if (b.DateAchat.Month == 12)
                    {
                        Inscriptions[11]++;
                    }
                }
            }

            return Inscriptions;
        }
        public async Task<List<int>>GetNombreInscritSemaine(string NomFestivalActuel, String Date)
        {
            HttpResponseMessage rep = client.GetAsync("api/Festivals/" + NomFestivalActuel + "/w/w").Result;
            var resp = await rep.Content.ReadAsStringAsync();
            int IdFestivalActuel = JsonConvert.DeserializeObject<int>(resp);

            HttpResponseMessage response = client.GetAsync("api/Billets/Festival/" + IdFestivalActuel).Result;
            ICollection<Billet> ListeBillets = null;
            if (response.IsSuccessStatusCode)
            {
                resp = await response.Content.ReadAsStringAsync();
                ListeBillets = JsonConvert.DeserializeObject<ICollection<Billet>>(resp);
            }
            List<int> Inscriptions = new List<int>();
            
            for(int i=1; i < 53; i++)
            {
                Inscriptions.Add(0);
            }

            foreach(Billet b in ListeBillets)
            {
                if (b.DateAchat.Year.ToString() == Date)
                {
                    int n = 0;
                    for (int i = 1; i < 365; i=i+7)
                    {
                        if(b.DateAchat.DayOfYear>=i && b.DateAchat.DayOfYear < i + 7)
                        {
                            Inscriptions[n]++;
                        }
                        n++;
                    }
                }
            }
            return Inscriptions;
        }
        public async Task<List<int>> GetNombreInscritJour(string NomFestivalActuel, String Date, int Mois)
        {
            //Recupere l'id du festival actuel
            HttpResponseMessage rep = client.GetAsync("api/Festivals/" + NomFestivalActuel + "/w/w").Result;
            var resp = await rep.Content.ReadAsStringAsync();
            int IdFestivalActuel = JsonConvert.DeserializeObject<int>(resp);

            HttpResponseMessage response = client.GetAsync("api/Billets/Festival/" + IdFestivalActuel).Result;
            ICollection<Billet> ListeBillets = null;
            if (response.IsSuccessStatusCode)
            {
                resp = await response.Content.ReadAsStringAsync();
                ListeBillets = JsonConvert.DeserializeObject<ICollection<Billet>>(resp);
            }
            List<int> Inscriptions = new List<int>();

            for (int i = 1; i < 32; i++)
            {
                Inscriptions.Add(0);
            }

            foreach (Billet b in ListeBillets)
            {
                if (b.DateAchat.Year.ToString() == Date)
                {
                    if (b.DateAchat.Month == Mois)
                    {
                        int n = 0;
                        for (int i = 1; i < 32; i++)
                        {
                            if (b.DateAchat.Day==i)
                            {
                                Inscriptions[n]++;
                            }
                            n++;
                        }
                    }
                   
                }
            }
            return Inscriptions;
        }
        //recuperer le billet
        public async Task<ICollection<Billet>> GetBilletsAsync()
        {
            ICollection<Billet> billets = new List<Billet>();
            HttpResponseMessage response = client.GetAsync("api/Billets").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                billets = JsonConvert.DeserializeObject<List<Billet>>(resp);
            }
            return billets;
        }
        //recupere festival par IDfestival
        public async Task<Festival> GetFestival(int Id)
        {
            Festival festival = null;
            HttpResponseMessage response = client.GetAsync("api/Festivals/Site/Web/" + Id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                festival = JsonConvert.DeserializeObject<Festival>(resp);
            }
            return festival;
        }
        public async Task<Festivalier> GetFestivalier(int IdFestivalier)
        {
            Festivalier festivalier = null;
            HttpResponseMessage response = client.GetAsync("api/Festivaliers/" + IdFestivalier).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                festivalier = JsonConvert.DeserializeObject<Festivalier>(resp);
            }
            return festivalier;
        }


    }
}