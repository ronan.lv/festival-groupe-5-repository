       
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using APIFestival.Models;
using APIFestival.Controllers;
namespace ClientLourdWPF
{
    /// <summary>
    /// Window1.xaml 的交互逻辑
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainwindow = new MainWindow();   //Login为窗口名，把要跳转的新窗口实例化
            Application.Current.Properties.Clear();
            mainwindow.Show();  //打开新窗口
            this.Close();  //关闭当前窗口
        }

        private void Btcreer_Click(object sender, RoutedEventArgs e)
        {

            if (Tbmdp.Text.Length >= 6 && Tbnom.Text.Length >0 && Tbpre.Text.Length > 0 && Tbnumero.Text.Length > 0 && Tbmail.Text.Length > 0)
            {
                Gestionnaire gestionnaire1 = ClientLourdWPF.ControllersAPI.API.Instance.GetGestionnaireByAdresseMail(Tbmail.Text).Result;
                if (gestionnaire1 == null)
                {
                    Gestionnaire gestionnaire = new Gestionnaire();



                    gestionnaire.MotDePasse = Tbmdp.Text;
                    gestionnaire.Nom = Tbnom.Text;
                    gestionnaire.Prenom = Tbpre.Text;
                    gestionnaire.AdresseMail = Tbmail.Text;
                    gestionnaire.NumeroTelephone = Tbnumero.Text;



                    _ = ClientLourdWPF.ControllersAPI.API.Instance.AjoutGestionnaire(gestionnaire);


                    MessageBox.Show("Gestionnaire ajoutée !", "Enregistrement effectué", MessageBoxButton.OK, MessageBoxImage.Information);
                    MainWindow mainwindow = new MainWindow();   //Login为窗口名，把要跳转的新窗口实例化
                    Application.Current.Properties.Clear();
                    mainwindow.Show();  //打开新窗口
                    this.Close();  //关闭当前窗口
                }
                else
                {
                    MessageBox.Show("Le AdresseMail de le gestionnaire  est deja existe !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else

                MessageBox.Show("Remplissez toutes les informations ou le mot de passe est trop court ", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);

              
        }

       
    }
}
