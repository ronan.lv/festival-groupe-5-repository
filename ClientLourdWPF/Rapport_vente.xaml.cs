﻿using APIFestival.Models;
using ClientLourdWPF.ControllersAPI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientLourdWPF
{
    /// <summary>
    /// Rapport_vente.xaml 的交互逻辑
    /// </summary>
    public partial class Rapport_vente : Window
    { List<KeyValuePair<string, int>> valueList1 = new List<KeyValuePair<string, int>>();
            List<KeyValuePair<string, int>> valueList2 = new List<KeyValuePair<string, int>>();
            List<KeyValuePair<string, int>> valueList3 = new List<KeyValuePair<string, int>>();
            List<KeyValuePair<string, int>> valueList4 = new List<KeyValuePair<string, int>>();
            List<KeyValuePair<string, int>> valueList5= new List<KeyValuePair<string, int>>();
        float[] ventes = new float[3000];
        float[] ventee = new float[3000];
        float[] ventec = new float[3000];
        float[] ventem = new float[3000];
        float[] venteo = new float[3000];
        int max = 0;
        int min= 9999;
        public Rapport_vente()
        {
            InitializeComponent();
            ImageBrush b = new ImageBrush();
            b.ImageSource = new BitmapImage(new Uri("ressources/window22.jpg", UriKind.Relative));
            b.Stretch = Stretch.Fill;
            this.Background = b;
            List<String> Countries = new List<string> { "Seine-Maritime", "Eure", "Calvados", "Manche", "Orne"};

            foreach (var p in Countries)
            {
                this.Cbdepar.Items.Add(p);
            }
             for (var n = 0; n < 3000; n++)
            {
                ventes[n] = 0;
                ventee[n] = 0;
                ventec[n] = 0;
                ventem[n] = 0;
                venteo[n] = 0;
            }
            List<Billet> Liste = new List<Billet>();
            Liste = (List<Billet>)API.Instance.GetBilletsAsync().Result;
            for (int k = 0; k < Liste.Count; k++)
            {//Seine-Maritime 76 ,Eure 27 ,Calvados 14 ,Manche 50 ,Orne 61
                var f = API.Instance.GetFestivalier(Liste[k].IdFestivalier).Result;
                if (f.CodePostal > 75999 && f.CodePostal < 77000)
                { ventes[Convert.ToInt32(Liste[k].DateAchat.Year.ToString())] += Convert.ToInt32(Liste[k].Montant.ToString());
                }
                else if (f.CodePostal > 26999 && f.CodePostal < 28000)
                {
                    ventee[Convert.ToInt32(Liste[k].DateAchat.Year.ToString())] += Convert.ToInt32(Liste[k].Montant.ToString());
                }
                else if (f.CodePostal > 13999 && f.CodePostal < 15000)
                {
                    ventec[Convert.ToInt32(Liste[k].DateAchat.Year.ToString())] += Convert.ToInt32(Liste[k].Montant.ToString());
                }
                else if (f.CodePostal > 49999 && f.CodePostal < 51000)
                {
                    ventem[Convert.ToInt32(Liste[k].DateAchat.Year.ToString())] += Convert.ToInt32(Liste[k].Montant.ToString());
                }
                else if (f.CodePostal > 60999 && f.CodePostal < 62000)
                {
                    venteo[Convert.ToInt32(Liste[k].DateAchat.Year.ToString())] += Convert.ToInt32(Liste[k].Montant.ToString());
                }
                else { int h = 0; } 
            }
           
            for (int k = 0; k < Liste.Count; k++)
            {
                Convert.ToInt32(Liste[k].DateAchat.Year.ToString());
                if (max < Convert.ToInt32(Liste[k].DateAchat.Year.ToString()))
                    max = Convert.ToInt32(Liste[k].DateAchat.Year.ToString());
                if (min > Convert.ToInt32(Liste[k].DateAchat.Year.ToString()))
                    min = Convert.ToInt32(Liste[k].DateAchat.Year.ToString());
               
            }
            for (int i = min; i <= max; i++)
            {
                Cbdebut.Items.Add(i);
                Cbfin.Items.Add(i);              
            }
        
        }
        private void showColumnChart1()
        { 

            if (Cbdebut.SelectedItem != null && Cbfin.SelectedItem != null && Cbdepar.SelectedItem != null)
            {
                int n = Convert.ToInt32(Cbdebut.SelectedItem.ToString());
                int m = Convert.ToInt32(Cbfin.SelectedItem.ToString());


                for (int j = n; j < m + 1; j++)
                {
                    valueList1.Add(new KeyValuePair<string, int>(j.ToString(), Convert.ToInt32(ventes[j])));
                                   
                }
                
                    
                  lineChart.DataContext = valueList1;
            }
            else
            {
                for (int j = 2000; j < 2005; j++)
                {
                    valueList1.Add(new KeyValuePair<string, int>(j.ToString(), 0));
                }
            }
            //折线图
            
        }
        private void showColumnChart2()
        {

            if (Cbdebut.SelectedItem != null && Cbfin.SelectedItem != null && Cbdepar.SelectedItem != null)
            {
                int n = Convert.ToInt32(Cbdebut.SelectedItem.ToString());
                int m = Convert.ToInt32(Cbfin.SelectedItem.ToString());


                for (int j = n; j < m + 1; j++)
                {
                    valueList2.Add(new KeyValuePair<string, int>(j.ToString(), Convert.ToInt32(ventee[j])));
                }

            }
            else
            {
                for (int j = 2000; j < 2005; j++)
                {
        
                    valueList2.Add(new KeyValuePair<string, int>(j.ToString(), Convert.ToInt32(ventee[j])));
                  
                }
            }
            //折线图
            lineChart.DataContext = valueList2;
        }
            private void showColumnChart3()
            {

                if (Cbdebut.SelectedItem != null && Cbfin.SelectedItem != null && Cbdepar.SelectedItem != null)
                {
                    int n = Convert.ToInt32(Cbdebut.SelectedItem.ToString());
                    int m = Convert.ToInt32(Cbfin.SelectedItem.ToString());


                    for (int j = n; j < m + 1; j++)
                    {
                        valueList3.Add(new KeyValuePair<string, int>(j.ToString(), Convert.ToInt32(ventec[j])));
               
                    }
                }
                else
                {
                    for (int j = 2000; j < 2005; j++)
                    {                      
                        valueList3.Add(new KeyValuePair<string, int>(j.ToString(), Convert.ToInt32(ventec[j])));
                       
                    }
                }
                //折线图
                lineChart.DataContext = valueList3;
            }
        private void showColumnChart4()
        {

            if (Cbdebut.SelectedItem != null && Cbfin.SelectedItem != null && Cbdepar.SelectedItem != null)
            {
                int n = Convert.ToInt32(Cbdebut.SelectedItem.ToString());
                int m = Convert.ToInt32(Cbfin.SelectedItem.ToString());


                for (int j = n; j < m + 1; j++)
                {
                    valueList4.Add(new KeyValuePair<string, int>(j.ToString(), Convert.ToInt32(ventem[j])));              
                }

            }
            else
            {
                for (int j = 2000; j < 2005; j++)
                {

                    valueList4.Add(new KeyValuePair<string, int>(j.ToString(), Convert.ToInt32(ventem[j])));

                }
            }
            //折线图
            lineChart.DataContext = valueList4;
        }
        private void showColumnChart5()
        {

            if (Cbdebut.SelectedItem != null && Cbfin.SelectedItem != null && Cbdepar.SelectedItem != null)
            {
                int n = Convert.ToInt32(Cbdebut.SelectedItem.ToString());
                int m = Convert.ToInt32(Cbfin.SelectedItem.ToString());


                for (int j = n; j < m + 1; j++)
                {
                    valueList5.Add(new KeyValuePair<string, int>(j.ToString(), Convert.ToInt32(venteo[j])));
                }

            }
            else
            {
                for (int j = 2000; j < 2005; j++)
                {

                    valueList5.Add(new KeyValuePair<string, int>(j.ToString(), Convert.ToInt32(venteo[j])));
                }
            }
            //折线图
            lineChart.DataContext = valueList5;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //  List<String> Countries = new List<string> { "Seine-Maritime", "Eure", "Calvados", "Manche", "Orne" };

            if (Cbdebut.SelectedItem != null && Cbfin.SelectedItem != null && Convert.ToInt32(Cbdebut.SelectedItem) < Convert.ToInt32(Cbfin.SelectedItem))
            {
                if (Cbdepar.SelectedItem.ToString() == "Seine-Maritime")
                {
                    
                    
                    showColumnChart1();
                }
                else if (Cbdepar.SelectedItem.ToString() == "Eure")
                {
                    
                    showColumnChart2();
                }
                else if (Cbdepar.SelectedItem.ToString() == "Calvados")
                {
                   
                    showColumnChart3();
                }
                else if (Cbdepar.SelectedItem.ToString() == "Manche")
                {
                    
                    showColumnChart4();
                }
                else if (Cbdepar.SelectedItem.ToString() == "Orne")
                {
                    
                    showColumnChart5();
                }
                else { int v = 0; }
            }
            else
            {
                MessageBox.Show("Respect le formule", "Erreur", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Window2 window2 = new Window2();   //Login为窗口名，把要跳转的新窗口实例化
            Application.Current.Properties.Clear();
            window2.Show();  //打开新窗口
            this.Close();  //关闭当前窗口
        }

      
    }

      
    }

